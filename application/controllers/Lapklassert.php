<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class lapklassert extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/lapklassert/';
        $this->_path_js = 'laporan/';
        $this->_judul = 'Laporan Klasifikasi Sertifikat';
        $this->_controller_name = 'lapklassert';
        $this->_model_name = 'model_lapklassert';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts']    = [$this->_path_js . $this->_controller_name];
        $data['tampil']   = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }

    public function response() //page: Response
    {
        $this->form_validation->set_rules('tanggalawal', 'tanggalawal', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tanggalakhir', 'tanggalakhir', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . 'form');
                $session_data = $this->session->userdata('logged_in');
                $user = $session_data['susrNama'];
                $tanggalawal = $data['tanggalawal'] = $this->input->post('tanggalawal');
                $tanggalakhir = $data['tanggalakhir'] = $this->input->post('tanggalakhir');
                $data['datas'] = $this->{$this->_model_name}->get($tanggalawal, $tanggalakhir);

                $data['page_judul'] = "Data Laporan Klasifikasi Sertifikat";
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }
}
