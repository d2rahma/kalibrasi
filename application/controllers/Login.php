<?php
defined('BASEPATH') or exit('No direct script access allowed');

class login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->_model_name      = 'model_reqkalibrasi';
		if ($this->session->userdata('logged_in') == true) //cek user logged
			redirect('home', 'refresh');
		$this->load->model($this->_model_name, '', true);
	}

	public function index()
	{
		$this->load->view('layouts/login');
	}
	public function tracking()
	{
		$this->form_validation->set_rules('nomorOrder', 'Nomor Tiket', 'required|trim|xss_clean');

		if ($this->form_validation->run() == TRUE) {
			$nomorOrder = $this->input->post('nomorOrder');
			$param = "permintaanNoOrder='" . $nomorOrder . "'";
			$datas = $this->{$this->_model_name}->permintaan($param, 'row');
			if ($datas != false) {
				$key = $this->encryptions->encode($nomorOrder, $this->config->item('encryption_key'));
				$result['status'] = 'success';
				$result['message'] = 'Nomor order ditemukan, mengalihkan halaman...';
				$result['redirect_url'] = base_url('login/cektiket/') . $key;
			} else {
				$result['status'] = 'danger';
				$result['message'] = 'Nomor order tidak ditemukan.';
			}
		} else {
			$result['status'] = 'danger';
			$result['message'] = validation_errors();
		}

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($result));
		echo $this->output->get_output();
		exit();
	}


	public function cektiket()
	{
		$id = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));	
		if (!empty($id)) {
			$param = "permintaanNoOrder='" . $id . "'";
			$permintaan = $this->{$this->_model_name}->permintaan($param, 'row');
			$history = $this->{$this->_model_name}->getHistory($permintaan->permintaanId);
			$data['datas']      = $permintaan;
			$data['history']    = $history;
			$data['loadpdf_url'] = site_url('login/loadpdf') . '/';
			$data['cetakterima']   = site_url('login/cetakterima') . '/' . $this->uri->segment(3);
			$this->load->view('layouts/tracking', $data);
		} else {
			redirect('login');
		}
	}

	public function loadpdf() //page: proses download pdf
	{
		$file = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
		$file = json_decode($file);
		$path = FCPATH . '../upload_files/' . $file[0] . '/' . $file[1];
		header('Content-type: application/pdf');
		header('Content-Disposition: inline; filename="' . $path . '"');
		header('Content-Transfer-Encoding: binary');
		header('Accept-Ranges: bytes');
		echo file_get_contents($path);
	}

	public function cetakterima()
    {
        $this->load->library('Qrcode_generator');
        $mpdf = new \Mpdf\Mpdf();
        $id = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $param = "permintaanNoOrder	='".$id."'"; 
        $permintaan = $this->{$this->_model_name}->permintaan($param,'row'); 
        $filename = $permintaan != false ? $permintaan->permintaanNoOrder : '';
        $this->qrcode_generator->generate((site_url('login/cektiket') . '/' .  $this->uri->segment(3)), $filename);       

        $pages = 'pages/reqkalibrasi/tanda_terima';
        $data['datas'] = $permintaan;
        $data['tanda_terima'] = site_url('reqkalibrasi/tanda_terima');

        $mpdf->WriteHTML($this->load->view($pages, $data, TRUE));

        $mpdf->Output();
    }
}
