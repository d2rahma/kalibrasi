<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refsertklasifikasi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refsertklasifikasi/';
        $this->_path_js = null;
        $this->_judul = 'Klasifikasi Sertifikat';
        $this->_controller_name = 'refsertklasifikasi';
        $this->_model_name = 'model_refsertklasifikasi';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('tb_sert_klasifikasi');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['sertkId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('tb_sert_klasifikasi', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $sertkIdOld = $this->input->post('sertkIdOld');
        if (empty($sertkIdOld))
            $this->form_validation->set_rules('sertkId', 'Kode', 'trim|xss_clean|required|is_unique[tb_sert_klasifikasi.sertkId]');
        else
            $this->form_validation->set_rules('sertkId', 'Kode', 'trim|xss_clean|required');
        $this->form_validation->set_rules('sertkNama', 'Nama', 'trim|xss_clean|required');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $sertkId = $this->input->post('sertkId');
                $sertkNama = $this->input->post('sertkNama');


                $param = array(
                    'sertkId' => $sertkId,
                    'sertkNama' => $sertkNama,

                );

                if (empty($sertkIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('tb_sert_klasifikasi', $param);
                } else {
                    $key = array('sertkId' => $sertkIdOld);
                    $proses = $this->{$this->_model_name}->update('tb_sert_klasifikasi', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['sertkId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('tb_sert_klasifikasi', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
