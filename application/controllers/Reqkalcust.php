<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class reqkalcust extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/reqkalcust/';
        $this->_path_js = null;
        $this->_judul = 'Pelanggan';
        $this->_controller_name = 'reqkalcust';
        $this->_model_name = 'model_reqkalcust';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('tb_customer');
        $data['create_url'] = site_url($this->_controller_name.'/create').'/';
        $data['update_url'] = site_url($this->_controller_name.'/update').'/';
        $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['customerId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('tb_customer',$key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {		
        $idCustomerOld = $this->input->post('customerIdOld');
        $this->form_validation->set_rules('customerNama','Nama','trim|xss_clean');
        $this->form_validation->set_rules('customerNoHp','Nomor HP','trim|xss_clean');
        $this->form_validation->set_rules('customerNoTelp','Nomor Telepon','trim|xss_clean');
        $this->form_validation->set_rules('customerEmail','Email','trim|xss_clean');
        $this->form_validation->set_rules('customerAlamat','Alamat','trim|xss_clean');

        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $customerNama = $this->input->post('customerNama');
                $customerNoHp = $this->input->post('customerNoHp');
                $customerNoTelp = $this->input->post('customerNoTelp');
                $customerEmail = $this->input->post('customerEmail');
                $customerAlamat = $this->input->post('customerAlamat');


                $param = array(
                    'customerNama'=>$customerNama,
                    'customerNoHp'=>$customerNoHp,
                    'customerNoTelp'=>$customerNoTelp,
                    'customerEmail'=>$customerEmail,
                    'customerAlamat'=>$customerAlamat,

                );

                if(empty($idCustomerOld))
                {
                    $proses = $this->{$this->_model_name}->insert('tb_customer',$param);
                } else {
                    $key = array('customerId'=>$idCustomerOld);
                    $proses = $this->{$this->_model_name}->update('tb_customer',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['customerId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('tb_customer',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }
}
