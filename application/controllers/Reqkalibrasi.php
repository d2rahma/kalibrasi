<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class reqkalibrasi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/reqkalibrasi/';
        $this->_path_js = null;
        $this->_judul = 'Permintaan Kalibrasi';
        $this->_controller_name = 'reqkalibrasi';
        $this->_model_name = 'model_reqkalibrasi';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts']    = [$this->_path_js . 'kalibrasi/permintaan'];
        $data['tambah'] = site_url($this->_controller_name . '/create');
        $data['tampil']   = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }


    public function response() //page: Response
    {
        $this->form_validation->set_rules('tanggalawal', 'tanggalawal', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tanggalakhir', 'tanggalakhir', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data['scripts']    = [$this->_path_js . 'kalibrasi/permintaan'];
                $session_data = $this->session->userdata('logged_in');
                $user = $session_data['susrNama'];
                $tanggalawal = $this->input->post('tanggalawal');
                $tanggalakhir = $this->input->post('tanggalakhir');
                $param = "permintaanTgl BETWEEN '" . $tanggalawal . " 00:00:00' AND '" . $tanggalakhir . " 23:59:59'";
                $permintaan = $this->{$this->_model_name}->permintaan($param, 'result');
                //echo $this->db->last_query();                   
                $data['datas'] = $permintaan;

                $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
                $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
                $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
                $data['verify_url'] = site_url($this->_controller_name . '/verify') . '/';
                $data['disposisi_url'] = site_url($this->_controller_name . '/disposisi') . '/';
                $data['detail_url'] = site_url($this->_controller_name . '/detail') . '/';
                $data['alat_url'] = site_url($this->_controller_name . '/alat') . '/';
                $data['page_judul'] = "Data Permintaan Kalibrasi";
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts']    = [$this->_path_js . 'kalibrasi/permintaan'];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['media'] = $this->{$this->_model_name}->get_ref_table('tb_pemintaan_media');
        $data['customer'] = $this->{$this->_model_name}->get_ref_table('tb_customer');
        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts']    = [$this->_path_js . 'kalibrasi/permintaan'];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['permintaanId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['media'] = $this->{$this->_model_name}->get_ref_table('tb_pemintaan_media');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {        
        $permintaanIdOld = $this->input->post('permintaanIdOld');
        $this->form_validation->set_rules('permintaanCustomerId', 'Nama Customer/Perusahaan', 'required|trim|xss_clean');
        $this->form_validation->set_rules('permintaanVia', 'permintaanVia', 'trim|xss_clean');
        $this->form_validation->set_rules('permintaanCatatan', 'permintaanCatatan', 'trim|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data['scripts']    = [$this->_path_js . 'kalibrasi/permintaan'];
                $session_data = $this->session->userdata('logged_in');
                $user = $session_data['susrNama'];
                $permintaanCustomerId = $this->input->post('permintaanCustomerId');
                $permintaanVia = $this->input->post('permintaanVia');
                $permintaanCatatan = $this->input->post('permintaanCatatan');                
                $alatNama = $this->input->post('alatNama');
                $alatJumlah = $this->input->post('alatJumlah');
                $alatSpesifikasi = $this->input->post('alatSpesifikasi');                   
                $alatPermintaan=$_POST['alatPermintaan'];
                $permintaanTgl = date("Y-m-d H:i:s");
                $permintaanStatus = '1';
                $permintaanNoOrder = generate_kode() . date('ymd');
                $permintaanId = auto_increment('tb_permintaan', 'permintaanId');

                $param = array(
                    'permintaanId' => $permintaanId,
                    'permintaanNoOrder' => $permintaanNoOrder,
                    'permintaanCustomerId' => $permintaanCustomerId,
                    'permintaanMediaId' => $permintaanVia,
                    'permintaanTgl' => $permintaanTgl,
                    'permintaanCatatan' => $permintaanCatatan,
                    'permintaanStatus' => $permintaanStatus
                );

                $param_his = array(
                    'historyPermintaanId' => $permintaanId,
                    'historyStatus' => $permintaanStatus,
                    'historyKeterangan' => 'Permintaan Telah Diterima',
                    'historyTanggal' => $permintaanTgl,
                    'historyUser' => $user
                );
                
                if(count($alatPermintaan) > 0)
                {
                    foreach ($alatPermintaan as $alat) { 
                        $param_alat  = array(
                            'alatPermintaanId' => $permintaanId,
                            'alatNama' => $alat['alatNama'],
                            'alatJumlah' => $alat['alatJumlah'],
                            'alatSpesifikasi' => $alat['alatSpesifikasi'],
                            'alatUser' => $user
                        );
                        $this->{$this->_model_name}->insert('tb_permintaan_alat', $param_alat);
                    }
                }

                if (empty($permintaanIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('tb_permintaan', $param);
                    if ($proses)
                        $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);                    
                } else {
                    $key = array('permintaanId' => $permintaanIdOld);
                    $proses = $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
                }

                if ($proses) {
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                } else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['permintaanId' => $keyS];
        $key2 = ['historyPermintaanId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('tb_permintaan', $key);
        if ($proses) {
            $this->{$this->_model_name}->delete('tb_permintaan_history', $key2);
            message($this->_judul . ' Berhasil Dihapus', 'success');
        } else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
    public function verify()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['permintaanId' => $keyS];
        $permintaanTgl = date("Y-m-d H:i:s");

        $param_his = array(
            'historyPermintaanId' => $keyS,
            'historyStatus' => '2',
            'historyKeterangan' => 'Permintaan Telah Diverifikasi Oleh Kesekretariatan',
            'historyTanggal' => $permintaanTgl,
            'historyUser' => $user
        );

        $param = array(
            'permintaanUserVerify' => $user,
            'permintaanVerify' => '1',
            'permintaanStatus' => '2'
        );

        $proses = $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);

        if ($proses) {
            $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
            message($this->_judul . ' Berhasil Diverifikasi', 'success');
        } else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Diverifikasi, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function disposisi()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['permintaanId' => $keyS];
        $permintaanTgl = date("Y-m-d H:i:s");

        $param_his = array(
            'historyPermintaanId' => $keyS,
            'historyStatus' => '3',
            'historyKeterangan' => 'Permintaan Telah Didisposisi oleh Kepala UPTD',
            'historyTanggal' => $permintaanTgl,
            'historyUser' => $user
        );

        $param = array(
            'permintaanUserDisposisi' => $user,
            'permintaanDisposisi' => '1',
            'permintaanStatus' => '3'
        );

        $proses = $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);

        if ($proses) {
            $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
            message($this->_judul . ' Berhasil Didisposisi', 'success');
        } else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Didisposisi, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function alat()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['permintaanId' => $keyS];
        $permintaanTgl = date("Y-m-d H:i:s");

        $param_his = array(
            'historyPermintaanId' => $keyS,
            'historyStatus' => '12',
            'historyKeterangan' => 'Alat telah dikembalikan ke pelanggan',
            'historyTanggal' => $permintaanTgl,
            'historyUser' => $user
        );

        $param = array(
            'permintaanUserDisposisi' => $user,
            'permintaanDisposisi' => '1',
            'permintaanStatus' => '12'
        );

        $proses = $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);

        if ($proses) {
            $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
            message($this->_judul . ' Status Alat Telah Diterima Pelanggan', 'success');
        } else {
            $error = $this->db->error();
            message($this->_judul . ' Status Alat Gagal Diterima Pelanggan, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function detail() //page: Detail
    {
        $session_data = $this->session->userdata('logged_in');
        $id = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data = $this->get_master($this->_path_page . 'detail');
        $param = "permintaanId='" . $id . "'";
        $permintaan = $this->{$this->_model_name}->permintaan($param, 'row');
        $noOrder = $this->encryptions->encode($permintaan->permintaanNoOrder, $this->config->item('encryption_key'));    
        $history = $this->{$this->_model_name}->getHistory($id);
        //echo $this->db->last_query();
        $data['datas']      = $permintaan;
        $data['history']    = $history;
        $data['user_group'] = $session_data;
        $data['loadpdf_url'] = site_url($this->_controller_name . '/loadpdf') . '/';
        $data['cetakterima']   = site_url($this->_controller_name . '/cetakterima') . '/' . $noOrder;
        $data['scripts']    = [$this->_path_js . 'kalibrasi/permintaan'];
        $this->load->view($this->_template, $data);
    }

    public function loadpdf() //page: proses download pdf
    {
        $file = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $file = json_decode($file);
        $path = FCPATH . '../upload_files/' . $file[0] . '/' . $file[1];        
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $path . '"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        echo file_get_contents($path);
    }

    public function cetakterima()
    {
        $this->load->library('Qrcode_generator');
        $mpdf = new \Mpdf\Mpdf();
        $id = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $param = "permintaanNoOrder='" . $id . "'";
        $permintaan = $this->{$this->_model_name}->permintaan($param, 'row');
        $filename = $permintaan != false ? $permintaan->permintaanNoOrder : '';
        $this->qrcode_generator->generate((site_url('login/cektiket') . '/' .  $this->uri->segment(3)), $filename);

        $pages = 'pages/reqkalibrasi/tanda_terima';
        $data['datas'] = $permintaan;
        $data['tanda_terima'] = site_url('reqkalibrasi/tanda_terima');

        $mpdf->WriteHTML($this->load->view($pages, $data, TRUE));

        $mpdf->Output();
    }
}
