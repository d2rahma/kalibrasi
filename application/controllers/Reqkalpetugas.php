<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class reqkalpetugas extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/reqkalpetugas/';
        $this->_path_js = null;
        $this->_judul = 'Petugas Kalibrasi';
        $this->_controller_name = 'reqkalpetugas';
        $this->_model_name = 'model_reqkalibrasi';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts']    = [$this->_path_js . 'kalibrasi/petugas'];
        $data['tampil']   = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }


    public function response() //page: Response
    {
        $this->form_validation->set_rules('tanggalawal', 'tanggalawal', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tanggalakhir', 'tanggalakhir', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {            
            if (IS_AJAX) {
                $data['scripts']    = [$this->_path_js . 'kalibrasi/petugas'];
                $session_data = $this->session->userdata('logged_in');
                $user = $session_data['susrNama'];                
                $tanggalawal = $this->input->post('tanggalawal');
                $tanggalakhir = $this->input->post('tanggalakhir');    
                $param = "permintaanDisposisi='1' AND permintaanTgl BETWEEN '".$tanggalawal." 00:00:00' AND '".$tanggalakhir." 23:59:59'";              
                $permintaan = $this->{$this->_model_name}->permintaan($param,'result');
                $data['datas'] = $permintaan;

                $data['create_url'] = site_url($this->_controller_name.'/create').'/';
                $data['update_url'] = site_url($this->_controller_name.'/update').'/';
                $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
                $data['detail_url'] = site_url($this->_controller_name.'/detail').'/';
                $data['tracking_url'] = site_url('reqkalibrasi/detail').'/';
                $data['start_url'] = site_url($this->_controller_name.'/start').'/';
                $data['done_url'] = site_url($this->_controller_name.'/form_selesai').'/';
                $data['page_judul'] = "Data petugas Kalibrasi";
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function create()
    {	
        $id = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts']    = [$this->_path_js . 'kalibrasi/petugas'];
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['permintaanId']=$id;   
        $data['catatan'] = false; 
        $data['petugas'] = $this->{$this->_model_name}->get_ref_table('pegawai','','pegawaiJabatan=6'); 
        $data['lokasi'] = $this->{$this->_model_name}->get_ref_table('tb_lokasi');       

        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts']    = [$this->_path_js . 'kalibrasi/petugas'];
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['permintaanId'=>$keyS];
        $data['permintaanId']=$keyS;
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['catatan'] = $this->{$this->_model_name}->get_history_petugas($key);    
        $data['petugas'] = $this->{$this->_model_name}->get_petugas($keyS);
        $data['lokasi'] = $this->{$this->_model_name}->get_ref_table('tb_lokasi');
        $this->load->view($this->_template, $data);
    }

    public function save()
    {		        
        $permintaanIdOld = $this->input->post('permintaanIdOld');        
        $this->form_validation->set_rules('permintaanTglAwalPengujian','Tanggal Awal Pengujian','required|trim|xss_clean|required');
        $this->form_validation->set_rules('permintaanTglAkhirPengujian','Tanggal Akhir Pengujian','required|trim|xss_clean|required');
        $this->form_validation->set_rules('petugasPegawaiId[]','Petugas Pengujian','trim|xss_clean|required');
        $this->form_validation->set_rules('permintaanLokasiPengujian','Lokasi Pengujian','trim|xss_clean|required');
        $this->form_validation->set_rules('historyCatatan','Catatan','trim|xss_clean');
        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $data['scripts']    = [$this->_path_js . 'kalibrasi/petugas'];
                $session_data = $this->session->userdata('logged_in');
                $user = $session_data['susrNama'];
                $permintaanTglAwalPengujian = $this->input->post('permintaanTglAwalPengujian');
                $permintaanTglAkhirPengujian = $this->input->post('permintaanTglAkhirPengujian');
                $petugasPegawaiId = $this->input->post('petugasPegawaiId');
                $permintaanLokasiPengujian = $this->input->post('permintaanLokasiPengujian');
                $historyCatatan = $this->input->post('historyCatatan');
                $petugasTgl = date("Y-m-d H:i:s");
                $petugasStatus='4'; 
                if(count($petugasPegawaiId)>0)
                {
                    $cekPetugas= $this->{$this->_model_name}->get_ref_table('tb_permintaan_petugas','',"petugasPermintaanId='".$permintaanIdOld."'");
                    if($cekPetugas)
                    {
                        $keyPetugas = ['petugasPermintaanId'=>$permintaanIdOld];
                        $this->{$this->_model_name}->delete('tb_permintaan_petugas',$keyPetugas);
                    }

                    foreach ($petugasPegawaiId as $pegawai) {     
                        $param = array(
                            'petugasPermintaanId'=>$permintaanIdOld,
                            'petugasPegawaiId'=>$pegawai,
                            'petugasTgl'=>$petugasTgl,
                            'petugasUser'=>$user
                        );                           
                        $proses=$this->{$this->_model_name}->insert('tb_permintaan_petugas',$param);
                    }

                    $param_permintaan = array(                         
                        'permintaanStatus'=>$petugasStatus,                    
                        'permintaanTglAwalPengujian'=>$permintaanTglAwalPengujian,
                        'permintaanTglAkhirPengujian'=>$permintaanTglAkhirPengujian,
                        'permintaanLokasiPengujian'=>$permintaanLokasiPengujian                       
                    );
                    $param_his_kaji = array( 
                        'historyPermintaanId'=>$permintaanIdOld,                   
                        'historyStatus'=>$petugasStatus,
                        'historyKeterangan'=>'Disposisi dan penunjukan petugas kalibrasi oleh Kasi PTPKSMB',
                        'historyTanggal'=>$petugasTgl,
                        'historyUser'=>$user,
                        'historyCatatan'=>$historyCatatan
                    );                                     
                                       
                    $keyPermintaan = ['permintaanId'=>$permintaanIdOld];
                    if($proses)
                    {        
                        $this->{$this->_model_name}->update('tb_permintaan',$param_permintaan,$keyPermintaan);
                        $this->{$this->_model_name}->insert('tb_permintaan_history',$param_his_kaji);            
                        message($this->_judul.' Berhasil Disimpan','success');
                    }
                    else
                    {
                        $error = $this->db->error();
                        message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                    }
                } else
                    message('Pilih Pegawai!! Minimal 1 Pegawai','error');                          
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['petugasId'=>$keyS];
        $key2 = ['historypetugasId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('tb_petugas',$key);        
        if ($proses) 
        {
            $this->{$this->_model_name}->delete('tb_petugas_history',$key2);
            message($this->_judul.' Berhasil Dihapus','success');
        }
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }

    public function start()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));        
        $key = ['permintaanId'=>$keyS];
        $mulaiTgl = date("Y-m-d H:i:s"); 

        $param_his = array( 
                    'historypermintaanId'=>$keyS,                   
                    'historyStatus'=>'5',
                    'historyKeterangan'=>'Petugas Mulai Proses Kalibrasi',
                    'historyTanggal'=>$mulaiTgl,
                    'historyUser'=>$user
                );

        $param = array(
                'permintaanKalStartUser'=>$user,
                'permintaanKalStart'=>'1',
                'permintaanStatus'=>'5'
                );

        $proses=$this->{$this->_model_name}->insert('tb_permintaan_history',$param_his);        
        
       if ($proses) 
       {
            $this->{$this->_model_name}->update('tb_permintaan',$param,$key);
            message($this->_judul.' Proses Kalibrasi Dimulai','success');
       }
       else
       {
            $error = $this->db->error();
            message($this->_judul.' Gagal Diverifikasi, '.$error['code'].': '.$error['message'],'error');
        }
    }  

    public function form_selesai()
    {
        $data = $this->get_master($this->_path_page . 'form_selesai');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));        
        $data['scripts']    = [$this->_path_js . 'kalibrasi/petugas'];
        $data['done_url'] = site_url($this->_controller_name . '/done') . '/';
        $data['status_page'] = 'Status Penyelesaian Alat';
        $key = ['permintaanId' => $keyS];
        $permintaan = $this->{$this->_model_name}->permintaanAlat($key, 'result');
        $data['datas'] = $permintaan;
        $this->load->view($this->_template, $data);
    }

    public function done()
    {      
        $permintaanId=$this->input->post('permintaanIdOld');
        $data['scripts']    = [$this->_path_js . 'kalibrasi/petugas'];
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $doneTgl = date("Y-m-d H:i:s");

        $cekModul = $this->input->post('cekModul');
        $alatJumlahSelesai= $this->input->post('alatJumlahSelesai');
        $alatJumlah= $this->input->post('alatJumlah');            
        $jumAlat=count($alatJumlahSelesai);         
        if($jumAlat>0)
        {
            for($i=0;$i<$jumAlat;$i++) {  
                 $alatSelesai=$alatJumlahSelesai[$i];
                 $alatJum=$alatJumlah[$i];
                 
                if($alatSelesai > $alatJum)
                {
                    message('Jumlah Alat Selesai Tidak Boleh Lebih Besar dari Jumlah Alat','error');
                    exit();
                }
                else
                {
                    $alatSelesai=$alatSelesai;
                    $idAlat=$cekModul[$i];
                    $param = array(
                       'alatJumlahSelesai'=>$alatSelesai
                    );
                    $key = array(
                       'alatPermintaanId'=>$permintaanId,
                       'alatId'=>$idAlat 
                    );
                    $this->{$this->_model_name}->update('tb_permintaan_alat',$param,$key);
                    
                }
            }
            $jumlahAlat=array_sum($alatJumlah);
            $jumlahAlatSelesai=array_sum($alatJumlahSelesai);
            $keyPermintaan = ['permintaanId'=>$permintaanId];

            if($jumlahAlatSelesai==$jumlahAlat)
            {
                $param_his = array( 
                    'historypermintaanId'=>$permintaanId,                   
                    'historyStatus'=>'6',
                    'historyKeterangan'=>'Petugas Telah Menyelesaikan Proses Kalibrasi '.$jumlahAlatSelesai.' Alat dari Jumlah '.$jumlahAlat.' Alat ',
                    'historyTanggal'=>$doneTgl,
                    'historyUser'=>$user
                );

                $param = array(
                    'permintaanKalDoneUser'=>$user,
                    'permintaanKalDone'=>'1',
                    'permintaanStatus'=>'6'
                );

                $proses=$this->{$this->_model_name}->insert('tb_permintaan_history',$param_his);        
                
               if ($proses) 
               {
                    $this->{$this->_model_name}->update('tb_permintaan',$param,$keyPermintaan);
                    message($this->_judul.'Proses Kalibrasi Telah Selesai','success');
               }
               else
               {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Diverifikasi, '.$error['code'].': '.$error['message'],'error');
                }
            }
            else
            {            
                $param_his = array( 
                    'historypermintaanId'=>$permintaanId,                   
                    'historyStatus'=>'6',
                    'historyKeterangan'=>'Petugas Telah Menyelesaikan Proses Kalibrasi '.$jumlahAlatSelesai.' Alat dari Jumlah '.$jumlahAlat.' Alat',
                    'historyTanggal'=>$doneTgl,
                    'historyUser'=>$user
                );
                $proses=$this->{$this->_model_name}->insert('tb_permintaan_history',$param_his);
                message($this->_judul.' Berhasil Disimpan','success');
            }
        }else
            message('Pilih Penyelesaian Alat!! Minimal 1','error');
        }       
    }