<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

use setasign\Fpdi\Tcpdf\Fpdi;

class reqkalsertifikat extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/reqkalsertifikat/';
        $this->_path_js = 'kalibrasi/';
        $this->_judul = 'Sertifikat';
        $this->_controller_name = 'reqkalsertifikat';
        $this->_model_name = 'model_reqkalibrasi';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts']    = [$this->_path_js . 'sertifikat'];
        $data['tambah'] = site_url($this->_controller_name . '/create');
        $data['tampil']   = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }

    public function response() //page: Response
    {
        $this->form_validation->set_rules('tanggalawal', 'tanggalawal', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tanggalakhir', 'tanggalakhir', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . 'form');
                $session_data = $this->session->userdata('logged_in');
                $user = $session_data['susrNama'];
                $data['susrSgroupNama'] = $session_data['susrSgroupNama'];

                $tanggalawal = $this->input->post('tanggalawal');
                $tanggalakhir = $this->input->post('tanggalakhir');
                $param = "permintaanDisposisi='1' AND permintaanTgl BETWEEN '" . $tanggalawal . " 00:00:00' AND '" . $tanggalakhir . " 23:59:59'";
                $data['datas'] = $this->{$this->_model_name}->permintaan($param, 'result');

                $data['draft_url'] = site_url($this->_controller_name . '/draft') . '/';
                $data['loadpdf_url'] = site_url($this->_controller_name . '/loadpdf') . '/';
                $data['verifymt_url'] = site_url($this->_controller_name . '/verifymt') . '/';
                $data['verifykp_url'] = site_url($this->_controller_name . '/verifykp') . '/';
                $data['verifypy_url'] = site_url($this->_controller_name . '/verifypy') . '/';
                $data['verifypyt_url'] = site_url($this->_controller_name . '/verifypyt') . '/';
                $data['acceptcust_url'] = site_url($this->_controller_name . '/acceptcust') . '/';
                $data['prosesSertifikat_url'] = site_url($this->_controller_name . '/prosesSertifikat') . '/';
                $data['selesaiSertifikat_url'] = site_url($this->_controller_name . '/selesaiSertifikat') . '/';
                $data['page_judul'] = "Data Permintaan Kalibrasi";
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function draft()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts']    = [$this->_path_js . 'sertifikat'];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['loadpdf_url'] = site_url($this->_controller_name . '/loadpdf') . '/';
        $data['status_page'] = 'Draft';
        $key = ['permintaanId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->permintaan($key, 'row');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $this->form_validation->set_rules('permintaanNoOrder', 'Nomor Order', 'trim|xss_clean');
        $this->form_validation->set_rules('permintaanNoSert', 'Nomor Sertifikat', 'trim|required|xss_clean');
        if (empty($_FILES['permintaanSertifikatDraft']['name']) and empty($rwpkIdOld))
            $this->form_validation->set_rules('permintaanSertifikatDraft', 'Draft Sertifikat', 'trim|xss_clean|required');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $session_data = $this->session->userdata('logged_in');
                $user = $session_data['susrNama'];
                $permintaanNoOrder = $this->input->post('permintaanNoOrder');
                $permintaanId = $this->input->post('permintaanId');
                $permintaanNoSert = $this->input->post('permintaanNoSert');
                $permintaanStatus = 7;
                $permintaanTgl = date('Y-m-d H:i:s');

                if (!empty($_FILES['permintaanSertifikatDraft']['name'])) {
                    $konfig = array(
                        'url' => '../upload_files/' . $permintaanNoOrder . '/',
                        'type' => 'pdf',
                        'size' => '15360',
                        'field' => 'permintaanSertifikatDraft',
                        'namafile' => 'Draft_' . $permintaanNoOrder . '_' . date("YmdHis")
                    );

                    $this->load->helper('uploadfile');
                    $file = uploadfile($konfig);

                    if (is_array($file)) :
                        $param = array(
                            'permintaanSertifikatDraft' => $file['file_name'],
                            'permintaanStatus' => $permintaanStatus,
                            'permintaanNoSert' => $permintaanNoSert
                        );

                        $param_his = array(
                            'historyPermintaanId' => $permintaanId,
                            'historyStatus' => $permintaanStatus,
                            'historyKeterangan' => 'Draft sertifikat telah selesai.',
                            'historyTanggal' => $permintaanTgl,
                            'historyUser' => $user
                        );

                        $key = array('permintaanId' => $permintaanId);
                        $proses = $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
                        $proses = $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);

                        if ($proses) {
                            message($this->_judul . ' Berhasil Disimpan', 'success');
                        } else {
                            $error = $this->db->error();
                            message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                        }
                    endif;
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function loadpdf() //page: proses download pdf
    {
        $file = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $file = json_decode($file);
        $path = FCPATH . '../upload_files/' . $file[0] . '/' . $file[1];
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $file[1] . '"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        echo file_get_contents($path);
    }

    public function loadklassert()
    {
        $id = substr($this->uri->segment(3), 0, 4);

        $datas = $this->{$this->_model_name}->get_by_id('tb_sert_klasifikasi', ['sertkId' => $id]);
        if ($datas != false)
            echo $datas->sertkNama;
    }

    public function verifypy()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $permintaanTgl = date("Y-m-d H:i:s");

        $param = array(
            'permintaanStatus' => 8
        );

        $param_his = array(
            'historyPermintaanId' => $keyS,
            'historyStatus' => 9,
            'historyKeterangan' => 'Verifikasi draft sertifikat telah selesai.',
            'historyTanggal' => $permintaanTgl,
            'historyUser' => $user
        );

        $key = array('permintaanId' => $keyS);
        $proses = $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
        $proses = $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);

        if ($proses) {
            message($this->_judul . ' Berhasil Diverifikasi oleh Penyelia', 'success');
        } else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Diverifikasi, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function verifypyt()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $permintaanTgl = date("Y-m-d H:i:s");

        $param = array(
            'permintaanStatus' => 6
        );

        $param_his = array(
            'historyPermintaanId' => $keyS,
            'historyStatus' => 9,
            'historyKeterangan' => 'Draft sertifikat ditolak, draft akan diperbaiki.',
            'historyTanggal' => $permintaanTgl,
            'historyUser' => $user
        );

        $key = array('permintaanId' => $keyS);
        $proses = $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
        $proses = $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);

        if ($proses) {
            message($this->_judul . ' Berhasil Ditolak oleh Penyelia', 'success');
        } else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Diverifikasi, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function verifymt()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $permintaanTgl = date("Y-m-d H:i:s");

        $param = array(
            'permintaanStatus' => 9
        );

        $param_his = array(
            'historyPermintaanId' => $keyS,
            'historyStatus' => 9,
            'historyKeterangan' => 'Sertifikat telah ditandatangani oleh manajer teknis.',
            'historyTanggal' => $permintaanTgl,
            'historyUser' => $user
        );

        $key = array('permintaanId' => $keyS);
        $proses = $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
        $proses = $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);

        if ($proses) {
            message($this->_judul . ' Berhasil Ditandatangani oleh Manajer Teknis', 'success');
        } else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Diverifikasi, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function verifykp()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));

        $key = array('permintaanId' => $keyS);
        $datas = $this->{$this->_model_name}->by_id($key);
        //echo $this->db->last_query();
        $permintaanTgl = date("Y-m-d H:i:s");
        $filename = 'Final_' . ($datas != false ? $datas->permintaanNoOrder : '') . '_' . date("YmdHis");

        $param = array(
            'permintaanStatus' => 10,
            'permintaanSertifikatFinal' => ($filename . '.pdf')
        );

        $param_his = array(
            'historyPermintaanId' => $keyS,
            'historyStatus' => 10,
            'historyKeterangan' => 'Sertifikat telah ditandatangani oleh Kepala UPTD.',
            'historyTanggal' => $permintaanTgl,
            'historyUser' => $user
        );

        $key = array('permintaanId' => $keyS);
        $proses = $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
        $proses = $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);

        if ($proses and $datas != false) {
            if(ob_get_length() > 0) {
                ob_clean();
            }
            //$certificate = '../upload_file/tcpdf.crt';
            $tcpdf = new FPDI();
            $tcpdf->setPrintHeader(false);
            $tcpdf->setPrintFooter(false);
            $tcpdf->SetMargins(0, 0, 0);
            $row = $tcpdf->setSourceFile('../upload_files/' . ($datas != false ? $datas->permintaanNoOrder : '') . '/' . ($datas != false ? $datas->permintaanSertifikatDraft : ''));

            $tcpdf->SetAuthor('Sertifkat Kalibrasi UPTD. BPSMB Kalimantan Timur');
            $tcpdf->SetTitle('Nomor Order: ' . ($datas != false ? $datas->permintaanNoOrder : ''));
            $tcpdf->SetSubject(($datas != false ? $datas->permintaanNoOrder : ''));

            for ($i = 1; $i <= $row; $i++) {
                $tplIdx = $tcpdf->importPage($i);
                $tcpdf->addPage();
                $tcpdf->useTemplate($tplIdx);
                if ($i == 1) {
                    // $info = array(
                    //     'Name' => $pegmNamaGelar,
                    //     'Location' => 'ID',
                    //     'Reason' => 'Otentikasi Dokumen PDF',
                    //     'ContactInfo' => 'UPT.TIK Universitas Mulawarman',
                    // );
                    // $tcpdf->setSignature(file_get_contents($certificate), file_get_contents($certificate), 'pdfdemo', '', 2, $info);

                    $url =  site_url('login/cektiket') . '/' .  $this->uri->segment(3);
                    //$message = 'Dokumen ini telah ditandatangani elektronik menggunakan domain <a href="' . $url . '">https://sidak.unmul.ac.id</a>.';

                    // if (!empty($pegttdAtasnama)) {
                    //     $tcpdf->SetFont('times', '', 12);
                    //     $tcpdf->Text(108, 209, 'a.n. ' . $pegttdAtasnama . ',');
                    // }

                    $tcpdf->SetFont('times', '', 12);
                    $tcpdf->Text(23, 210, 'Mengetahui');
                    $tcpdf->Text(23, 214, 'Kepala,');
                    $tcpdf->write2DBarcode($url, 'QRCODE,L', 24, 221, 25, 25);
                    $tcpdf->SetFont('times', 'BU', 12);
                    $tcpdf->Text(23, 247, 'Atikah, SE');
                    $tcpdf->SetFont('times', '', 12);
                    $tcpdf->Text(23, 251, 'NIP. 19641213 199103 2 006');

                    $tcpdf->Text(135, 214, 'Manajer Teknis,');
                    $tcpdf->write2DBarcode($url, 'QRCODE,L', 136, 221, 25, 25);
                    $tcpdf->SetFont('times', 'BU', 12);
                    $tcpdf->Text(135, 247, 'Sutaji, SE');
                    $tcpdf->SetFont('times', '', 12);
                    $tcpdf->Text(135, 251, 'NIP. 19660423 198902 1 002');
                    //$tcpdf->SetFont('times', '', 8);
                    //$tcpdf->writeHTMLCell(100, 20, 116, 255, $message, 0, 0, 0, true, '', true);
                    //$tcpdf->Image(APPPATH . 'cert/tte4.jpg', 117, 201, 60, 18, 'JPG');
                    //$tcpdf->setSignatureAppearance(117, 221, 25, 25);
                }
            }

            $tcpdf->Output(APPPATH . '../upload_files/' . ($datas != false ? $datas->permintaanNoOrder : '') . '/' . $filename  . '.pdf', 'F');

            message($this->_judul . ' Berhasil Ditandatangani oleh Kepala UPTD', 'success');
        } else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Diverifikasi, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function acceptcust()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $permintaanTgl = date("Y-m-d H:i:s");

        $param = array(
            'permintaanStatus' => 11
        );

        $param_his = array(
            'historyPermintaanId' => $keyS,
            'historyStatus' => 11,
            'historyKeterangan' => 'Sertifikat telah diserahkan dan diterima oleh pelanggan.',
            'historyTanggal' => $permintaanTgl,
            'historyUser' => $user
        );

        $key = array('permintaanId' => $keyS);
        $proses = $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
        $proses = $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);

        if ($proses) {
            message($this->_judul . ' Berhasil diproses oleh Petugas', 'success');
        } else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal diproses, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function prosesSertifikat()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $permintaanTgl = date("Y-m-d H:i:s");

        $param = array(
            'permintaanStatus' => 13
        );

        $param_his = array(
            'historyPermintaanId' => $keyS,
            'historyStatus' => 13,
            'historyKeterangan' => 'Dalam proses pembuatan sertifikat',
            'historyTanggal' => $permintaanTgl,
            'historyUser' => $user
        );

        $key = array('permintaanId' => $keyS);
        $proses = $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
        $proses = $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);

        if ($proses) {
            message($this->_judul . ' Berhasil Diproses', 'success');
        } else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Diproses, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }

    public function selesaiSertifikat()
    {
        $session_data = $this->session->userdata('logged_in');
        $user = $session_data['susrNama'];
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $permintaanTgl = date("Y-m-d H:i:s");

        $param = array(
            'permintaanStatus' => 14
        );

        $param_his = array(
            'historyPermintaanId' => $keyS,
            'historyStatus' => 14,
            'historyKeterangan' => 'Sertifikat telah selesai. Dan siap untuk diambil pelanggan.',
            'historyTanggal' => $permintaanTgl,
            'historyUser' => $user
        );

        $key = array('permintaanId' => $keyS);
        $proses = $this->{$this->_model_name}->update('tb_permintaan', $param, $key);
        $proses = $this->{$this->_model_name}->insert('tb_permintaan_history', $param_his);

        if ($proses) {
            message($this->_judul . ' Berhasil Diproses', 'success');
        } else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Diproses, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
