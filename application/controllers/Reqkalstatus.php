<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class reqkalstatus extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/reqkalstatus/';
        $this->_path_js = null;
        $this->_judul = 'Reqkalstatus';
        $this->_controller_name = 'reqkalstatus';
        $this->_model_name = 'model_reqkalstatus';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('tb_pemintaan_status');
        $data['create_url'] = site_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = site_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = site_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = site_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['statusId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('tb_pemintaan_status', $key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $statusIdOld = $this->input->post('statusIdOld');
        $this->form_validation->set_rules('statusNama', 'statusNama', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $statusNama = $this->input->post('statusNama');


                $param = array(
                    'statusNama' => $statusNama,

                );

                if (empty($statusIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('tb_pemintaan_status', $param);
                } else {
                    $key = array('statusId' => $statusIdOld);
                    $proses = $this->{$this->_model_name}->update('tb_pemintaan_status', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['statusId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('tb_pemintaan_status', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
