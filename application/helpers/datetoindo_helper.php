<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('datetoindo'))
{
	function datetoindo($date){
		if($date!='0000-00-00') {
			$BulanIndo = array("Januari", "Februari", "Maret",
				"April", "Mei", "Juni",
				"Juli", "Agustus", "September",
				"Oktober", "Nopember", "Desember");

			$tahun = substr($date, 0, 4);
			$bulan = substr($date, 5, 2);
			$tgl   = substr($date, 8, 2);
			
			$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
			return($result);
		} else
		return(false);
	}
	function daytoindo($date){
		$hari = $date;

		switch($hari){
			case 'Sun':
			$hari_ini = "Minggu";
			break;

			case 'Mon':			
			$hari_ini = "Senin";
			break;

			case 'Tue':
			$hari_ini = "Selasa";
			break;

			case 'Wed':
			$hari_ini = "Rabu";
			break;

			case 'Thu':
			$hari_ini = "Kamis";
			break;

			case 'Fri':
			$hari_ini = "Jumat";
			break;

			case 'Sat':
			$hari_ini = "Sabtu";
			break;

			default:
			$hari_ini = "Tidak di ketahui";		
			break;
		}
		return $hari_ini;
	}
}
?>