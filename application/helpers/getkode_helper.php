<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('auto_increment'))
{
	function auto_increment($table,$field)
	{
		// Get a reference to the controller object
	    $CI = get_instance();

	    // You may need to load the model if it hasn't been pre-loaded
		$CI->load->model('model_master','',TRUE);
		$last_id = $CI->model_master->get_by_last_id($table,$field);
		//echo $CI->db->last_query();exit();

		if(!empty($last_id))
		{
			$id = $last_id->$field + 1;
		}
		else
			$id = '1';
		
		return $id;
	}
}

if (!function_exists('generate_kode')) {
	function generate_kode()
	{
		$karakter = 'QWERTYUIOPASDFGHJKLZXCVBNM';
		$kode = '';
		for ($i = 0; $i < 5; $i++) {
			$pos = rand(0, strlen($karakter) - 1);
			$kode .= $karakter[$pos];
		}

		return $kode;
	}
}