<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Mpdf\QrCode\QrCode;
use Mpdf\QrCode\Output;

class QrCode_generator
{
	function generate($content, $filename)
	{
		$qrCode = new QrCode($content);
		$output = new Output\Png();

		// Save black on white PNG image 100px wide to filename.png
		file_put_contents((FCPATH . '../upload_files/qrcode/' . $filename . '.png'), $output->output($qrCode, 255, [255, 255, 255], [0, 0, 0]));
	}
}
