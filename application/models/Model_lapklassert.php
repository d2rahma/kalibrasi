<?php
class Model_lapklassert extends Model_Master
{
    protected $table = 'tb_permintaan';


    public function __construct()
    {
        parent::__construct();
    }

    function get($awal, $akhir)
    {
        $this->db->select('sertkId,sertkNama,COUNT(permintaanId) jumPermintaan');
        $this->db->from('tb_sert_klasifikasi');
        $this->db->join('tb_permintaan', "sertkId = LEFT(permintaanNoSert,4) AND permintaanTgl BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59' AND permintaanStatus >= 10", 'LEFT');
        $this->db->group_by('sertkId');
        $this->db->order_by('sertkId');

        $qr = $this->db->get();

        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }
}
