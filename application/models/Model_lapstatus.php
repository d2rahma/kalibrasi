<?php
class Model_lapstatus extends Model_Master
{
    protected $table = 'tb_permintaan';


    public function __construct()
    {
        parent::__construct();
    }

    function get($awal, $akhir)
    {
        $this->db->select('statusId,statusNama,COUNT(permintaanId) jumPermintaan');
        $this->db->from('tb_pemintaan_status');
        $this->db->join('tb_permintaan', "statusId = permintaanStatus AND permintaanTgl BETWEEN '" . $awal . " 00:00:00' AND '" . $akhir . " 23:59:59'", 'LEFT');
        $this->db->group_by('statusId');
        $this->db->order_by('statusId');

        $qr = $this->db->get();

        if ($qr->num_rows() > 0)
            return $qr->result();
        else
            return false;
    }
}
