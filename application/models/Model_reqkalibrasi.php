<?php
class Model_reqkalibrasi extends Model_Master
{
    protected $table = 'tb_permintaan';


    public function __construct()
    {
        parent::__construct();
    }       

    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('tb_pemintaan_status','permintaanStatus = statusId','LEFT');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('tb_pemintaan_status','permintaanStatus = statusId','LEFT');
        $this->db->where($id);
        $qr = $this->db->get();
        if ($qr->num_rows() == 1)
            return $qr->row();
        else
            return false;
    }

    function get_petugas($id)
    {
        $this->db->select('*');
        $this->db->from('pegawai');
        $this->db->join('tb_permintaan_petugas','pegawaiId = petugasPegawaiId and petugasPermintaanId='.$id.'','LEFT');
        $this->db->where('pegawaiJabatan','6');

        $qr=$this->db->get();

        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function get_history_petugas($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('tb_permintaan_history','historyPermintaanId = permintaanId and historyStatus=4','LEFT');
        $this->db->where($id);

        $qr=$this->db->get();

        if($qr->num_rows()>0)
            return $qr->row();
        else
            return false;
    }

    function permintaanAlat($where, $mode)
    {
        $this->db->select("petugasPermintaanId,GROUP_CONCAT(pegawaiNama SEPARATOR '; ') petugas,GROUP_CONCAT(pegawaiId SEPARATOR '; ') penguji", FALSE);
        $this->db->from('tb_permintaan_petugas');
        $this->db->join('pegawai', 'petugasPegawaiId = pegawaiId', 'LEFT');
        $this->db->group_by('petugasPermintaanId');

        $subquery = $this->db->get_compiled_select();
        
        $this->db->select('*');
        $this->db->from('tb_permintaan');
        $this->db->join('tb_pemintaan_status', 'permintaanStatus = statusId', 'LEFT');
        $this->db->join('tb_customer', 'customerId = permintaanCustomerId', 'LEFT');
        $this->db->join('(' . $subquery . ') datas', 'petugasPermintaanId=permintaanId', 'LEFT');
        $this->db->join('tb_permintaan_alat', 'alatPermintaanId=permintaanId', 'LEFT');
        $this->db->order_by('permintaanTgl', 'desc');
        if (!empty($where))
            $this->db->where($where);

        $qr = $this->db->get();
        if ($qr->num_rows() > 0) {
            return $mode == 'result' ? $qr->result() : $qr->row();
        } else
            return false;
    }
}
