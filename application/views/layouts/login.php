<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

<!-- begin::Head -->

<head>
	<base href="../../../">
	<meta charset="utf-8" />
	<title><?= base_url(); ?> | Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!--begin::Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

	<!--end::Fonts -->

	<!--begin::Page Custom Styles(used by this page) -->
	<link href="<?= base_url(); ?>assets/css/pages/login/login-3.css" rel="stylesheet" type="text/css" />

	<!--end::Page Custom Styles -->

	<!--begin::Global Theme Styles(used by all pages) -->
	<link href="<?= base_url(); ?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url(); ?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

	<!--end::Global Theme Styles -->

	<!--begin::Layout Skins(used by all pages) -->
	<link href="<?= base_url(); ?>assets/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url(); ?>assets/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url(); ?>assets/css/skins/brand/dark.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url(); ?>assets/css/skins/aside/dark.css" rel="stylesheet" type="text/css" />

	<!--end::Layout Skins -->
	<link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/silakasicon.png" />
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

	<!-- begin:: Page -->
	<div class="kt-grid kt-grid--ver kt-grid--root">
		<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="min-height: 100%;
    min-width: 1280px;
    width: 100%;
    height: auto;
    position: fixed;
    top: 0;
    left: 0;background-image: url(<?= base_url(); ?>assets/media/bg/bg-3-gd.jpg);">				
				<div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
					<div class="kt-login__container">
						<div class="kt-login__logo">
							<a href="<?= base_url()?>">
								<img src="<?= base_url(); ?>assets/media/logos/silakas.jpg" width='300' height='100'>
							</a>
							<div class="kt-login__head">								
								<h5>UPTD Balai Pengujian dan Sertifikasi Mutu Barang </h5>
							</div>
						</div>

						<div class="kt-login__signin" id="kt-login--signin">							
							<form class="kt-form" action="<?= base_url() . 'otentifikasi' ?>" method="post" id="form_validation">
								<div class="input-group">
									<input class="form-control" type="text" placeholder="Username" name="username" autocomplete="off">
								</div>
								<div class="input-group">
									<input class="form-control" type="password" placeholder="Password" name="password">
								</div>
								<div class="kt-login__actions">
                                    <a href="#" class="kt-link kt-login__link-forgot">
                                        &nbsp;
                                    </a>
                                    <button id="kt_signin_submit" class="btn btn-primary btn-elevate kt-login__btn-primary">Submit</button>
                                </div>								

							</form>
						</div>

						<div class="kt-login__track" id="kt-login--track">
                            <!--begin::Form-->
                            <form class="kt-form" action="<?= base_url('login') . '/tracking' ?>" method="POST" novalidate="novalidate" id="kt_track_form">
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Masukan Nomor Order" name="nomorOrder" autocomplete="off">
                                </div>
                                <!--begin::Action-->
                                <div class="kt-login__actions">
                                    <a href="#" class="kt-link kt-login__link-forgot">
                                        &nbsp;
                                    </a>
                                    <button id="kt_track_submit" class="btn btn-primary btn-elevate kt-login__btn-primary">Submit</button>
                                </div>

                                <!--end::Action-->
                            </form>
                            <!--end::Form-->
                        </div>
                        <br>	
                        <div class="kt-login__divider">
                            <div class="kt-divider">
                                <span></span>
                                <span>OR</span>
                                <span></span>
                            </div>
                        </div>
                        <br>	
                        <!--end::Divider-->

                        <!--begin::Options-->
                        <div class="kt-login__options" align="center">
                            <button id="kt_signin" class="btn btn-success btn-elevate kt-login__btn-primary">
                                <i class="fa fa-key"></i>
                                Sign In
                            </button>
                            <button id="kt_tracking" class="btn btn-warning btn-elevate kt-login__btn-primary">
                                <i class="fa fa-check-double"></i>
                                Lacak Permintaan
                            </button>
                        </div>

                        <!--end::Options-->



					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- end:: Page -->

	<!-- begin::Global Config(global config for global JS sciprts) -->
	<script>
		var KTAppOptions = {
			"colors": {
				"state": {
					"brand": "#5d78ff",
					"dark": "#282a3c",
					"light": "#ffffff",
					"primary": "#5867dd",
					"success": "#34bfa3",
					"info": "#36a3f7",
					"warning": "#ffb822",
					"danger": "#fd3995"
				},
				"base": {
					"label": [
						"#c5cbe3",
						"#a1a8c3",
						"#3d4465",
						"#3e4466"
					],
					"shape": [
						"#f0f3ff",
						"#d9dffa",
						"#afb4d4",
						"#646c9a"
					]
				}
			}
		};
	</script>

	<!-- end::Global Config -->

	<!--begin::Global Theme Bundle(used by all pages) -->
	<script src="<?= base_url(); ?>assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
	<script src="<?= base_url(); ?>assets/js/scripts.bundle.js" type="text/javascript"></script>

	<!--end::Global Theme Bundle -->

	<!--begin::Page Scripts(used by this page) -->
	<script src="<?= base_url(); ?>assets/js/pages/custom/pages/user/login.js" type="text/javascript"></script>

	<!--end::Page Scripts -->
</body>

<!-- end::Body -->

</html>