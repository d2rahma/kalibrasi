<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->

<head>
    <base href="../../">
    <meta charset="utf-8" />
    <title>Kalibrasi</title>
    <meta name="description" content="No aside layout examples">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <!--end::Fonts -->

    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Page Vendors Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="assets/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/skins/brand/light.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />

    <!-- optionally if you need to use a theme, then include the theme CSS file as mentioned below -->
    <link href="assets/css/theme-rating.css" media="all" rel="stylesheet" type="text/css" />
    

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/media/logos/silakasicon.png" />
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-page--loading">

    <!-- begin:: Page -->

    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="index.html">
                <img alt="Logo" src="assets/media/logos/logo-dark.png" />
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
        </div>
    </div>

    <!-- end:: Header Mobile -->
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <!-- begin:: Header -->
                <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

                    <!-- begin:: Header Menu -->
                    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                        <div class="kt-header-logo">
                            <a href="<?= base_url();?>">
                                
                                <div style="font-size:18px;font-weight:bold;color:black" id="kt-aside__brand-name" class="kt-hidden-mobile">
                                    <img src="<?= base_url(); ?>assets/media/logos/silakas.jpg" width='200' height='60'>
                                    &nbsp Sistem Informasi Layanan Kalibrasi </div>
                            </a>
                        </div>
                    </div>

                    <!-- end:: Header Menu -->

                    <!-- begin:: Header Topbar -->
                    <div class="kt-header__topbar">

                        <!--begin: User Bar -->
                        <div class="kt-header__topbar-item kt-header__topbar-item--user">
                            <div class="kt-header__topbar-user">
                                <a href="<?=base_url();?>" title="Login"class="btn btn-primary btn-elevate ">
                                    <i class="la la-user"></i> Login
                                </a>
                                        
                            </div>
                        </div>

                        <!--end: User Bar -->
                    </div>

                    <!-- end:: Header Topbar -->
                </div>

                <!-- end:: Header -->

                    <!-- begin:: Subheader -->

                    <!-- end:: Subheader -->

                   <!-- begin:: Content -->
                    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title">
                                                Detail Permintaan Kalibrasi
                                            </h3>
                                        </div>
                                        <div class="kt-portlet__head-toolbar">
                                            <div class="kt-portlet__head-group">

                                                <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
                                                <a href="#" data-ktportlet-tool="reload" class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-refresh"></i></a>
                                                <a href="#" data-ktportlet-tool="remove" class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-close"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body">
                                        <div class="kt-portlet__content">
                                            <p id="ticketId"><label style="font-weight: bold">Nomor Tracking :</label>
                                                <?= $datas->permintaanNoOrder ?></p>
                                            <p id="ticketId"><label style="font-weight: bold">Nama Customer/Perusahaan :</label>
                                                <?= $datas->customerNama ?></p>
                                            <p id="ticketId"><label style="font-weight: bold">Detail Alat :</label>
                                                <?= $datas->detailAlat ?></p>                        
                                            <p><label style="font-weight: bold">Tanggal Permintaan :</label>
                                                <?= datetoindo($datas->permintaanTgl) . ' ' . date('h:i A', strtotime($datas->permintaanTgl)) ?></p>
                                            <?php if (!empty($datas->petugas)) : ?>
                                                <p><label style="font-weight: bold">Petugas Kalibrasi :</label>
                                                <?= $datas->petugas ?>                            
                                            <?php endif; ?><p>
                                            <p><label style="font-weight: bold">Status Permintaan :</label>
                                            <?= $datas->statusNama ?></p>
                                            <p><a href="<?=$cetakterima?>" class="btn btn-instagram" target='_blank'><i class="la la-print"></i> Print Bukti</a></p>
                                            <br><br>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <?php  /** ?>                                     
                                        if (!empty($datas->permintaanSertifikatFinal)) {
                                            $keyFinal = $this->encryptions->encode(json_encode([$datas->permintaanNoOrder, $datas->permintaanSertifikatFinal]), $this->config->item('encryption_key'));
                                        ?>
                                            <a href="<?= $loadpdf_url . $keyFinal ?>" title="Download Final"class="btn btn-success">
                                                <span>
                                                    <i class="la la-file"></i> Download Sertifikat</a>
                                                </span>
                                            </a>
                                        <?php
                                        }
                                        ?>     
                                        <?php **/ ?>                                   
                                    </div>
                                </div>            
                            </div>
                            <div class="col-lg-6">
                                <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title">
                                                Riwayat Permintaan Kalibrasi
                                            </h3>
                                        </div>
                                        <div class="kt-portlet__head-toolbar">
                                            <div class="kt-portlet__head-group">
                                                <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body">
                                        <div class="kt-notes">
                                            <div class="kt-notes__items">
                                                <?php if (!empty($history)) :
                                                    foreach ($history as $value) { ?>                                  
                                                        <div class="kt-notes__item kt-notes__item--clean">
                                                            <div class="kt-notes__media">
                                                                <span class="kt-notes__circle"></span>
                                                            </div>
                                                            <div class="kt-notes__content">
                                                                <div class="kt-notes__section">
                                                                    <div class="kt-notes__info">
                                                                        <!-- <a href="#" class="kt-notes__title">
                                                                    </a> -->
                                                                        <span class="kt-notes__desc">
                                                                            <b><?= datetoindo($value->historyTanggal) . ' ' . date('h:i A', strtotime($value->historyTanggal)) ?></b>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <span class="kt-notes__body">
                                                                    <?= $value->historyKeterangan ?>
                                                                </span>
                                                            </div>
                                                        </div>
                                                <?php }
                                                endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                    

                    <!-- end:: Content -->

                <!-- begin:: Footer -->
                <?php $this->load->view('layouts/footer'); ?>


                <!-- end:: Footer -->
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>

    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="/assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
    <script src="/assets/js/scripts.bundle.js" type="text/javascript"></script>
    <!--begin::Page Scripts(used by this page) -->    
    <script src="/assets/js/star-rating.min.js" type="text/javascript"></script>

    <script src="/assets/js/themes-rating.js"></script>

    <script type="text/javascript">
        const KTTicketing = function () {
        const main_form = $('#main_form');
        const initHandleWidgets = () => {        
            $('.kv-uni-star').rating({
                theme: 'krajee-uni',
                filledStar: '&#x2605;',
                emptyStar: '&#x2606;'
            });
        }
        const initHandleShow = () => {                        
            $('.kv-uni-star').on('change', function () {
                const ticketId = $('#ticketId').text();
                $.ajax({
                    type:'POST',
                    url:'/cektiket/rating',
                    data:{rating:$(this).val(),nomorTiket:ticketId},
                    success: data => {
                        console.log(data)
                        swal.fire({title: "Indeks Kepuasan Masyarakat", text: 'Terimakasih Telah Mengisi IKM, Berkas Permintaan Anda Telah Kami Kirimkan. Mohon Periksa Email Anda!', type: 'success'});
                    }
                });
            });
        }

        return {
               init: function () {
                    initHandleWidgets();
                    initHandleShow();
                }
            };
        }();

        KTUtil.ready(function () {
            KTTicketing.init();
        });
    </script>
    <!--end::Page Scripts -->
</body>

<!-- end::Body -->

</html>