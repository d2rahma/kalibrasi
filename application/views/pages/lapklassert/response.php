<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">

                <div class="kt-portlet__body">
                    <div class="kt-portlet__head-title">
                        <h5><?= strtoupper($page_judul) ?></h5>
                    </div>
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Kode</th>
                                            <th>Jenis Sertifikat</th>
                                            <th>Jumlah Permintaan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            $jum = 0;
                                            foreach ($datas as $row) {

                                                $key = json_encode(['sertkId' => $row->sertkId, 'tanggalawal' => $tanggalawal, 'tanggalakhir' => $tanggalakhir]);
                                                $key = $this->encryptions->encode($key, $this->config->item('encryption_key'));
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->sertkId ?></td>
                                                    <td><?= $row->sertkNama ?></td>
                                                    <td style="text-align: right;"><?= $row->jumPermintaan ?></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                        <?php
                                                $jum += $row->jumPermintaan;
                                            }
                                        ?>
                                            <tr>
                                                <td colspan="3">TOTAL</td>
                                                <td style="text-align: right;"><?= $jum ?></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->