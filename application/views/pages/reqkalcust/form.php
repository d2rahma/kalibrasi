
            <!-- BEGIN: Subheader -->
            <?php $this->load->view('layouts/subheader'); ?>
            <!-- END: Subheader -->

            <!--Begin::Row-->
            <!-- begin:: Content -->
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div id="response"></div>
                        <!--begin::Portlet-->
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <?=strtoupper($page_judul)?>
                                    </h3>
                                </div>
                            </div>

                            <!--begin::Form-->
                            <form class="kt-form" action="<?=$save_url?>" method="post" id="form_form">
                                <div class="kt-portlet__body">
                                    <input type="hidden" name="customerIdOld" value="<?=$datas!=false?$datas->customerId:''?>">
                                    
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="customerNama" placeholder="Nama" aria-describedby="Nama" value="<?=$datas!=false?$datas->customerNama:''?>">
                                </div>
                            
                                <div class="form-group">
                                    <label>Nomor HP</label>
                                    <input type="text" class="form-control" name="customerNoHp" placeholder="Nomor HP" aria-describedby="Nomor HP" value="<?=$datas!=false?$datas->customerNoHp:''?>">
                                </div>
                            
                                <div class="form-group">
                                    <label>Nomor Telepon</label>
                                    <input type="text" class="form-control" name="customerNoTelp" placeholder="Nomor Telepon" aria-describedby="Nomor Telepon" value="<?=$datas!=false?$datas->customerNoTelp:''?>">
                                </div>
                            
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="customerEmail" placeholder="Email" aria-describedby="Email" value="<?=$datas!=false?$datas->customerEmail:''?>">
                                </div>
                            
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input type="text" class="form-control" name="customerAlamat" placeholder="Alamat" aria-describedby="Alamat" value="<?=$datas!=false?$datas->customerAlamat:''?>">
                                </div>
                            
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </form>

                            <!--end::Form-->
                        </div>

                        <!--end::Portlet-->
                    </div>
                </div>
            </div>
            <!--End::Row-->
            