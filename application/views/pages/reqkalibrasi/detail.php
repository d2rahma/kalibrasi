<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->
<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">

        <div class="col-lg-6">
            <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Detail Permintaan Kalibrasi
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">

                            <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
                            <a href="#" data-ktportlet-tool="reload" class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-refresh"></i></a>
                            <a href="#" data-ktportlet-tool="remove" class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-close"></i></a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-portlet__content">
                        <p id="ticketId"><label style="font-weight: bold">Nomor Tracking :</label>
                            <?= $datas->permintaanNoOrder ?></p>
                        <p id="ticketId"><label style="font-weight: bold">Nama Pelanggan/Perusahaan :</label>
                            <?= $datas->customerNama ?></p>
                        <p id="ticketId"><label style="font-weight: bold">Detail Alat :</label>
                            <?= $datas->detailAlat ?></p>                                        
                        <p><label style="font-weight: bold">Tanggal Permintaan :</label>
                            <?= datetoindo($datas->permintaanTgl) . ' ' . date('h:i A', strtotime($datas->permintaanTgl)) ?></p>
                        <?php if (!empty($datas->petugas)) : ?>
                            <p><label style="font-weight: bold">Petugas Kalibrasi :</label>
                            <?= $datas->petugas ?>                            
                        <?php endif; ?><p>
                        <p><label style="font-weight: bold">Status Permintaan :</label>
                        <?= $datas->statusNama ?></p>
                        <p><a href="<?=$cetakterima?>" class="btn btn-instagram" target='_blank'><i class="la la-print"></i> Print Bukti</a></p>
                        <br><br>


                    </div>
                </div>
                <div class="kt-portlet__foot">                    
                    <?php                                       
                        if (!empty($datas->permintaanSertifikatFinal)) {
                            $keyFinal = $this->encryptions->encode(json_encode([$datas->permintaanNoOrder, $datas->permintaanSertifikatFinal]), $this->config->item('encryption_key'));
                        ?>
                            <a href="<?= $loadpdf_url . $keyFinal ?>" title="Download Final"class="btn btn-success">
                                <span>
                                    <i class="la la-file"></i> Download Sertifikat</a>
                                </span>
                            </a>
                        <?php
                        }
                        ?> 
                </div>
            </div>            
        </div>
        <div class="col-lg-6">
            <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Riwayat Permintaan Kalibrasi
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-notes">
                        <div class="kt-notes__items">
                            <?php if (!empty($history)) :
                                foreach ($history as $value) { ?>                                  
                                    <div class="kt-notes__item kt-notes__item--clean">
                                        <div class="kt-notes__media">
                                            <span class="kt-notes__circle"></span>
                                        </div>
                                        <div class="kt-notes__content">
                                            <div class="kt-notes__section">
                                                <div class="kt-notes__info">
                                                    <!-- <a href="#" class="kt-notes__title">
                                                </a> -->
                                                    <span class="kt-notes__desc">
                                                        <b><?= datetoindo($value->historyTanggal) . ' ' . date('h:i A', strtotime($value->historyTanggal)) ?></b>
                                                    </span>
                                                </div>
                                            </div>
                                            <span class="kt-notes__body">
                                                <?= $value->historyKeterangan ?>
                                            </span>
                                        </div>
                                    </div>
                            <?php }
                            endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>
<!--End::Row-->