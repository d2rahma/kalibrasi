<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_permintaan" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="permintaanIdOld" value="<?= $datas != false ? $datas->permintaanId : '' ?>">

                        <div class="form-group">
                            <label>Nama Customer/Perusahaan</label>
                            <select class="form-control m-select2" name="permintaanCustomerId">
                                <option value=""></option>
                                <?php
                                foreach ($customer as $row) {
                                    echo '<option value="' . $row->customerId . '"' . ($datas != false ? ($datas->permintaanCustomerId == $row->customerId ? 'selected' : '') : '') . '>' . $row->customerNama . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Permintaan Via</label>
                            <select class="form-control m-select2" name="permintaanVia">
                                <option value=""></option>
                                <?php
                                foreach ($media as $row) {
                                    echo '<option value="' . $row->mediaId . '"' . ($datas != false ? ($datas->permintaanMediaId == $row->mediaId ? 'selected' : '') : '') . '>' . $row->mediaNama . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Pesan</label>
                            <input type="text" class="form-control" name="permintaanCatatan" placeholder="Catatan/Pesan" aria-describedby="permintaanCatatan" value="<?= $datas != false ? $datas->permintaanCatatan : '' ?>">
                        </div>
                    </div>
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                               Permintaan Kalibrasi Alat
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div id="kt_repeater_3">                            
                            <div data-repeater-list="alatPermintaan" class="col-lg-12">
                                <div data-repeater-item="" class="form-group row align-items-center">
                                    <div class="form-group col-md-8">                                       
                                        <label>Nama Alat </label>
                                        <input name="alatNama" type="text" class="form-control" placeholder="Nama Alat"  />                                        
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label>Jumlah Alat</label>
                                        <input name="alatJumlah" type="text" class="form-control" placeholder="Jumlah Alat" />
                                    </div>
                                    <div class="form-group col-md-10">      
                                        <label>Spesifikasi Alat</label>
                                        <textarea name="alatSpesifikasi" rows="3" class="form-control"></textarea>                                        
                                    </div>    
                                    <div class="col-md-2">
                                        <a href="javascript:;" data-repeater-delete="" class="btn btn-danger">
                                        <i class="la la-trash-o"></i>Delete</a>
                                    </div>
                                </div>                                
                            </div>
                            <div class="form-group">
                                <div class="col-lg-3"></div>
                                <div class="col">
                                    <div data-repeater-create="" class="btn btn-success">
                                    <i class="la la-plus"></i>Add</div>
                                </div>
                            </div>
                        </div>
                    </div>    
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_form" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->