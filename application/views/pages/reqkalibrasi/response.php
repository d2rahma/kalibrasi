<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                
                <div class="kt-portlet__body">                    
                    <div class="kt-portlet__head-title">
                        <h5><?= strtoupper($page_judul) ?></h5>
                    </div>                    
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>                                            
                                            <th>Nomor Order</th>
                                            <th>Identitas Pelanggan</th>
                                            <th>Nama Alat -<br> Jumlah Alat</th>        
                                            <th>Tanggal Permintaan</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php                                        
                                        if($datas!=false)
                                        {
                                            $i = 1;
                                            foreach($datas as $row)
                                            {

                                                $key = $this->encryptions->encode($row->permintaanId,$this->config->item('encryption_key'));
                                                ?>
                                                <tr>
                                                    <th scope="row"><?=$i++?></th>                   
                                                    <td><?=$row->permintaanNoOrder?></td>
                                                    <td><?=$row->customerNama.'<br>'.$row->customerNoHp.'<br>'.$row->customerEmail?></td>  
                                                    <td><?=$row->permintaanNamaAlat.'- <br>'.$row->permintaanJumlahAlat?></td>                           
                                                    <td><?=$row->permintaanTgl?></td>
                                                    <td><?=$row->statusNama?></td>
                                                    <td>
                                                        <?php if($row->permintaanVerify<>'1')
                                                            {
                                                        ?>
                                                        <a href="<?=$verify_url.$key?>" title="Verifikasi" id='ts_verify_row<?= $i; ?>' class="ts_verify_row btn btn-sm btn-outline-success btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fas fa-check"></i>
                                                            </span>
                                                        </a>   
                                                        <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </span>
                                                        </a>
                                                        <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-trash-alt"></i>
                                                            </span>
                                                        </a>     
                                                        <?php
                                                            }
                                                        else if($row->permintaanDisposisi<>'1' and $row->permintaanVerify=='1')
                                                            {
                                                        ?>
                                                        <a href="<?=$disposisi_url.$key?>" title="Disposisi" id='ts_disposisi_row<?= $i; ?>' class="ts_disposisi_row btn btn-sm btn-outline-success btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-thumbs-up"></i>
                                                            </span>
                                                        </a>
                                                        <?php }?>
                                                         <a href="<?=$detail_url.$key?>" title="Detail Permintaan"  class="btn btn-sm btn-outline-success btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa flaticon2-fast-next"></i>
                                                            </span>
                                                        </a>     
                                                        <?php
                                                        if($row->permintaanKalDone=='1' and $row->permintaanStatus<>'12')
                                                        {
                                                        ?>
                                                            <a href="<?=$alat_url.$key?>" title="Alat Diterima Pelanggan"  id='ts_alat_row<?= $i; ?>' class="ts_alat_row btn btn-sm btn-outline-warning btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fa-tools"></i>
                                                                </span>
                                                            </a>  
                                                        <?php 
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>
            
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
