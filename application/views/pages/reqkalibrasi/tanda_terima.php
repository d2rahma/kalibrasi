<!DOCTYPE html>
<html lang="en">

<head>
  <title></title>
  <style>
    body {
      font-family: "bookmanoldstyle";
      line-height: 1.5;
      font-size: 10px;
      text-align: left;
    }

    .title {
      font-size: 16pt;
    }

    .text-center {
      text-align: center
    }

    .underline {
      text-decoration: underline;
    }

    .bold {
      font-weight: bold;
    }

    .uppercase {
      text-transform: uppercase;
      font-family: arial;
      font-size: 14px;
    }

    .m-0 {
      margin: 0;
    }

    .paragraf {
      text-align: justify;
      font-size: 10pt
    }

    .subheader {
      text-align: center;
      font-size: 8pt
    }

    .mb-10 {
      margin-bottom: 10px
    }

    .mt-30 {
      margin-top: 30px;
    }

    .sub-title {
      font-size: 11pt;
    }

    .elem-center {
      padding-left: 1cm;

    }

    .capitalize {
      text-transform: capitalize;
    }

    .height-5 {
      height: 5cm
    }

    td {
      vertical-align: top;
    }

    .footer {
      font-size: 6pt;
    }

    .borderbottom {
      border-bottom: double;
    }

    .garis_tepi1 {
      border: 1px solid;
      padding: 5px;
    }

    .dt {
      border: 1px solid;
      padding: 5px;
      border-collapse: collapse;
    }
  </style>
</head>

<body>
  <table align="center">
    <tr align="center">
      <td rowspan="4"><img src="<?php echo base_url(); ?>assets/media/logos/logo_login.png" width="80" height="90" /><br /></td>
      <td class="uppercase text-center">PEMERINTAH PROVINSI KALIMANTAN TIMUR</td>
      <td rowspan="4"><img src="<?php echo base_url(); ?>assets/media/logos/kan.png" width="100" height="80" /><br /></td>
    </tr>
    <tr align="center">
      <td class="uppercase text-center">DINAS PERINDUSTRIAN PEDAGANGAN, KOPERASI DAN UMKM</td>
    </tr>
    <tr align="center">
      <td class="bold uppercase text-center">UPTD BALAI PENGUJIAN DAN SERTIFIKASI MUTU BARANG</td>
    </tr>
    <tr align="center">
      <td class="capitalize subheader text-center">Jl. Letjen M.T. Haryono No. 45 Samarinda Telp./Fax. (0541) 733731 Kotak Pos 75126</td>
    </tr>
  </table>

  <div class="borderbottom"></div>
  <div class="garis_tepi1">
    <table align="center" width="100%" class="dt">
      <tr align="center">
        <td colspan="5" align="right" class="dt"> F.a PTLK-PK</td>
      </tr>
      <tr class="dt">
        <td rowspan="2" colspan="2" class="uppercase text-center title bold dt">BUKTI PENERIMAAN KALIBRASI</td>
        <td class="subheader" align="left">No. Pesanan</td>
        <td class="subheader" align=" left">: <?= $datas->permintaanNoOrder ?></td>
      </tr>
      <tr class="dt">
        <td class="subheader" align="left">Tanggal Terima</td>
        <td class="subheader" align="left">: <?= datetoindo($datas->permintaanTgl) ?></td>
      </tr>
    </table>
    <table align="center" width="100%" class="dt">
      <tr class="subheader dt">
        <td class="subheader dt" align="left">Pelanggan</td>
        <td colspan="3" class="subheader dt" align=" left">: <?= $datas->customerNama ?></td>
        <td class="subheader dt" align="left">No. Surat</td>
        <td class="subheader dt" align=" left">: ___________ </td>
      </tr>
      <tr class="subheader dt">
        <td class="subheader dt" align="left">Alamat</td>
        <td colspan="3" align="left" class="subheader dt">: <?= $datas->customerAlamat ?></td>
        <td class="subheader dt" align="left">Tanggal</td>
        <td class="subheader dt" align=" left">: ___________</td>
      </tr>
      <tr class="subheader dt">
        <td class="subheader dt" align="left">Kontak Personil</td>
        <td class="subheader dt" align="left">: <?= $datas->customerNoHp ?></td>
        <td class="subheader dt" align="left">Telp.</td>
        <td class="subheader dt" align="left">: <?= $datas->customerNoTelp ?></td>
        <td colspan="2" rowspan="2" class="subheader dt" align="left">Disetujui Oleh Pelanggan <br><br> <u><?= $datas->customerNama ?></u></td>
      </tr>
      <tr class="subheader dt">
        <td class="subheader dt" align="left">Pelaksanaan Kalibrasi</td>
        <td colspan="3" class="subheader dt" align="left">: <?= $datas->lokasiNama ?></td>
      </tr>
    </table>
    <br>
    <!-- <table align="center" width="100%" class="dt">
      <tr class="subheader dt">
        <td class="uppercase text-center title bold dt" colspan="5">DAFTAR PERMINTAAN</td>
      </tr>
      <tr class="subheader dt">
        <td class="subheader dt" align="left">No.</td>
        <td class="subheader dt" align="left">Nama Barang</td>
        <td class="subheader dt" align="left">Spesifikasi</td>
        <td class="subheader dt" align="left">Jumlah</td>
        <td class="subheader dt" align="left">Keterangan</td>
        <td class="subheader dt" align="left">No. Sertifikat</td>
      </tr>
      <tr class="subheader dt">
        <td class="subheader dt" align="left">1</td>
        <td class="subheader dt" align="left"><?= $datas->permintaanNamaAlat ?><br><br><br><br><br><br><br><br><br><br><br><br></td>
        <td class="subheader dt" align="left"><?= $datas->permintaanSpesifikasiAlat ?></td>
        <td class="subheader dt" align="left"><?= $datas->permintaanJumlahAlat ?></td>
        <td class="subheader dt" align="left"><?= $datas->permintaanCatatan ?></td>
      </tr>
      <tr class="subheader dt">
        <td class="subheader dt" align="right" colspan="3">Jumlah Pesanan</td>
        <td class="subheader dt" align="left"><?= $datas->permintaanJumlahAlat ?></td>
        <td class="subheader dt" align="left"><?= $datas->permintaanCatatan ?></td>
      </tr>
    </table>
    <br>
    <table align="center" width="100%" class="dt">
      <tr class="subheader dt">
        <td colspan="2" class="subheader dt bold" align="left">Pemeriksaan Kesesuaian permintaan oleh personil Lab </td>
        <td class="subheader dt bold" align="left">Nama Paraf</td>
      </tr>
      <tr class="subheader dt">
        <td class="subheader dt" align="left">
          <input type="checkbox">
          Pengecekan Visual
        </td>
        <td class="subheader dt" align="left">_______________</td>
        <td class="subheader dt" align="left">_______________</td>
      </tr>
      <tr class="subheader dt">
        <td class="subheader dt" align="left">
          <input type="checkbox">
          Kesesuaian Ruang Lingkup
        </td>
        <td class="subheader dt" align="left">_______________</td>
        <td class="subheader dt" align="left">_______________</td>
      </tr>
      <tr class="subheader dt">
        <td class="subheader dt" align="left">
          <input type="checkbox">
          Metode
        </td>
        <td class="subheader dt" align="left">_______________</td>
        <td class="subheader dt" align="left">_______________</td>
      </tr>
    </table> -->
    <div class="garis_tepi1">
      <div class="bold">Perhatian :</div>
      <div>
        <ul>
          <li>Apabila dalam uraian permintaan tersebut tidak memenuhi spesifikasi teknis, setelah dikaji ulang permintaan maka akan diberitahukan (selambat-lambatnya 2 hari kerja)</li>
          <li>Permintaan dan sertifikasi kalibrasi dapat diambil/diserahkan dengan memperhatikan bukti permintaan kalibrasi</li>
          <li>Pembayaran dapat dilakukan secara tunai atau transfer melalui Kas Daerah Pemerintah Provinsi Kaltim</li>
          <li>Untuk pelacakan proses kalibrasi, dapat diakses melalui laman https://silakas.bpsmb.indagkop.kaltimprov.go.id/</li>
        </ul>
      </div>
      <table border="0" align="right" cellpadding="0" cellspacing="0">
        <tr class="subheader">
          <td class="subheader" align="center">Penerima,</td>
        </tr>
        <tr class="subheader">
          <td class="subheader" align="center">Sekretaris
        </tr>
        <tr class="subheader">
          <td class="subheader" align="center">
            <img width="125" src="../upload_files/qrcode/<?= $datas->permintaanNoOrder ?>.png" alt="">
          </td>
        </tr>
      </table>
      <!-- <table class="dt" width="50%" cellpadding="5">
        <tr class="dt">
          <td colspan="2" class="bold">Distribusi</td>
        </tr>
        <tr class="dt">
          <td>Lembar Putih</td>
          <td>: Pelanggan</td>
        </tr>
        <tr class="dt">
          <td>Lembar Kuning</td>
          <td>: Bendahara</td>
        </tr>
        <tr class="dt">
          <td>Lembar Biru</td>
          <td>: Sekertaris</td>
        </tr>
      </table> -->
    </div>
  </div>
</body>

</html>