<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->                
                <div class="kt-portlet__body">
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_permintaan">    
                    <input type="hidden" name="permintaanIdOld" value="<?=$permintaanId!=false?$permintaanId:''?>">
                    <div class="form-group">
                        <label>Tanggal Kalibrasi</label>
                        <!-- <div class="col-lg-12 col-md-9 col-sm-12"> -->
                            <div class="input-daterange input-group" id="kt_datepicker_5">
                                <input type="text" class="form-control" name="permintaanTglAwalPengujian" value="<?=$datas!=false?$datas->permintaanTglAwalPengujian:''?>"/>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                </div>
                                <input type="text" class="form-control" name="permintaanTglAkhirPengujian" value="<?=$datas!=false?$datas->permintaanTglAkhirPengujian:''?>"/>
                            </div>
                            <span class="form-text text-muted">Tanggal Awal dan Tanggal Akhir Kalibrasi</span>                                 
                        </div>
                        <div class="form-group">
                            <label>Pilih Petugas</label>

                            <select class="form-control selectpicker" multiple="multiple" name="petugasPegawaiId[]">
                                <?php
                                foreach($petugas as $row){
                                    echo '<option value="'.$row->pegawaiId.'"'.(!empty($row->petugasId)? 'selected' : '') . '>'.$row->pegawaiNama.'</option>';
                                }
                                ?>
                            </select>

                        </div>
                        <div class="form-group">
                            <label>Pilih Lokasi Kalibrasi</label>
                            <select class="form-control m-select2" name="permintaanLokasiPengujian">
                                <option value=""></option>
                                <?php 
                                foreach($lokasi as $row){
                                    echo '<option value="'.$row->lokasiId.'"'.($datas!=false?$datas->permintaanLokasiPengujian == $row->lokasiId ? 'selected' : '' : '') . '>'.$row->lokasiNama.'</option>';
                                }                                    
                                ?>
                            </select>                                    
                        </div>
                        <div class="form-group">
                            <label>Catatan</label>
                            <input type="text" class="form-control" name="historyCatatan" placeholder="Catatan/Pesan" aria-describedby="historyCatatan" value="<?=$catatan!=false?$catatan->historyCatatan:''?>">
                        </div>                                
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_form" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                

                <!--end::Form-->                
                </form>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
    <!--End::Row-->

