<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->                
                <div class="kt-portlet__body">
                <form class="kt-form" action="<?=$done_url?>" method="post" id="form_penyelesaian">    
                    <input type="hidden" name="permintaanIdOld" value="<?=$datas!=false?$datas[0]->permintaanId:''?>">                    
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label>Nomor Order</label>
                            <input type="text" class="form-control" name="permintaanNoOrder" placeholder="Nomor Order" aria-describedby="permintaanNoOrder" value="<?=$datas!=false?$datas[0]->permintaanNoOrder:''?>" <?=$datas!=false?'readonly':''?>>
                        </div>
                        <div class="col-lg-4">
                            <label>Nama Pelanggan/Perusahaan</label>
                            <input type="text" class="form-control" name="customerNama" placeholder="Nama Perusahaan" aria-describedby="customerNama" value="<?=$datas!=false?$datas[0]->customerNama:''?>" <?=$datas!=false?'readonly':''?>>
                        </div>
                        <div class="col-lg-4">
                            <label>Tanggal Permintaan</label>
                            <input type="text" class="form-control" name="permintaanTgl" placeholder="Tanggal Permintaan" aria-describedby="permintaanTgl" value="<?=$datas!=false?$datas[0]->permintaanTgl:''?>" <?=$datas!=false?'readonly':''?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Petugas Kalibrasi</label>
                        <input type="text" class="form-control" name="petugas" placeholder="Petugas Kalibrasi" aria-describedby="petugas" value="<?=$datas!=false?$datas[0]->petugas:''?>" <?=$datas!=false?'readonly':''?>>
                    </div>   
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Alat</th>
                                            <th>Jumlah Alat</th>
                                            <th>Jumlah Alat Selesai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            foreach ($datas as $row) {
                                                if (!empty($row->alatId))
                                                    $cek = "checked";
                                                else
                                                    $cek="";
                                                ?>
                                                <tr>
                                                    <th scope="row"><input type="checkbox" class="checked" <?=$cek?> name="cekModul[]" value="<?=$row->alatId?>"/></th>
                                                    <td>
                                                        <input type="text" class="form-control" name="alatNama" placeholder="Nama Alat" aria-describedby="alatNama" value="<?=$row->alatNama?>">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="alatJumlah form-control" name="alatJumlah[]" placeholder="Jumlah Alat" aria-describedby="alatJumlah" value="<?=$row->alatJumlah?>">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="alatJumlahSelesai form-control" name="alatJumlahSelesai[]" placeholder="Jumlah Alat Selesai" aria-describedby="alatJumlahSelesai" value="<?=$row->alatJumlahSelesai?>">
                                                    </td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>                             
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" id="btn_form" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>                
                <!--end::Form-->                
                </form>
                </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>