<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                
                <div class="kt-portlet__body">                    
                    <div class="kt-portlet__head-title">
                        <h5><?= strtoupper($page_judul) ?></h5>
                    </div>                    
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>                                            
                                            <th>Nomor Order</th>
                                            <th>Tanggal Mulai Kalibrasi</th>
                                            <th>Tanggal Akhir Kalibrasi</th>
                                            <th>Petugas Kalibrasi</th> 
                                            <th>Lokasi Kalibrasi</th>  
                                            <th>Status</th>           
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if($datas!=false)
                                        {
                                            $i = 1;
                                            foreach($datas as $row)
                                            {

                                                $key = $this->encryptions->encode($row->permintaanId,$this->config->item('encryption_key'));
                                                ?>
                                                <tr>
                                                    <th scope="row"><?=$i++?></th>                   
                                                    <td>  
                                                        <a href="<?=$tracking_url.$key?>"><?=$row->permintaanNoOrder?></a></td>
                                                    <td><?=$row->permintaanTglAwalPengujian?></td>  
                                                    <td><?=$row->permintaanTglAkhirPengujian?></td> 
                                                    <td><?=$row->petugas?></td>  
                                                    <td><?=$row->lokasiNama?></td>    
                                                    <td><?=$row->statusNama?></td>
                                                    <td>
                                                        <?php if(empty($row->permintaanTglAwalPengujian)){?>
                                                        <a href="<?=$create_url.$key?>" title="Tambah Petugas" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="flaticon2-plus"></i>
                                                            </span>
                                                        </a>                                            
                                                        <?php 
                                                        } 
                                                        else
                                                            {
                                                            if($row->permintaanStatus==4 and $row->permintaanKalStart<>'1'){?> 
                                                            <a href="<?=$start_url.$key?>" title="Mulai Kalibrasi" id='ts_start_row<?= $i; ?>' class="ts_start_row btn btn-sm btn-outline-success btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fa-sign-in-alt"></i>
                                                                </span>
                                                            </a>               
                                                        <?php } 
                                                            if($row->permintaanStatus==5 and $row->permintaanKalDone<>'1'){?> 
                                                            <a href="<?=$done_url.$key?>" title="Penyelesaian Alat" class="btn btn-sm btn-outline-success btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fa-vote-yea"></i>
                                                                </span>
                                                            </a>               
                                                        <?php }?>
                                                        <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </span>
                                                        </a>
                                                        <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-trash-alt"></i>
                                                            </span>
                                                        </a>
                                                    <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>
            
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
