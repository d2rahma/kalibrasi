<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?= $save_url ?>" method="post" id="form_data" enctype="multipart/form-data">
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>Nomor Order</label>
                            <input type="text" class="form-control" name="permintaanNoOrder" readonly value="<?= $datas != false ? $datas->permintaanNoOrder : '' ?>">
                            <input type="hidden" name="permintaanId" readonly value="<?= $datas != false ? $datas->permintaanId : '' ?>">
                        </div>
                        <div class="form-group">
                            <label>Nama Pelanggan/Perusahaan</label>
                            <input type="text" class="form-control" readonly name="permintaanNamaCust" placeholder="Nama Customer/Perusahaan" aria-describedby="permintaanNamaCust" value="<?= $datas != false ? $datas->customerNama : '' ?>">
                        </div>
                        <div class="form-group">
                            <label>Pesan</label>
                            <input type="text" class="form-control" readonly name="permintaanCatatan" placeholder="Catatan/Pesan" aria-describedby="permintaanCatatan" value="<?= $datas != false ? $datas->permintaanCatatan : '' ?>">
                        </div>
                        <div class="form-group">
                            <label>Nama Alat</label>
                            <input type="text" class="form-control" readonly name="permintaanNamaAlat" placeholder="Nama Alat" aria-describedby="permintaanNamaAlat" value="<?= $datas != false ? $datas->permintaanNamaAlat : '' ?>">
                        </div>
                        <div class="form-group">
                            <label>Jumlah Alat</label>
                            <input type="text" class="form-control" readonly name="permintaanJumlahAlat" placeholder="Jumlah Alat" aria-describedby="permintaanJumlahAlat" value="<?= $datas != false ? $datas->permintaanJumlahAlat : '' ?>">
                        </div>
                        <div class="form-group">
                            <label>Nomor Sertifikat</label>
                            <input type="text" class="form-control" id="permintaanNoSert" name="permintaanNoSert" placeholder="Nomor Sertifikat" aria-describedby="Nomor Sertifikat" value="<?= $datas != false ? $datas->permintaanNoSert : '' ?>">
                        </div>
                        <div class="form-group">
                            <label>Klas. Sertifikat</label>
                            <input type="text" class="form-control" id="klasSertifikat" placeholder="Klasifikasi Sertifikat" aria-describedby="Klasifikasi Sertifikat" value="<?= $datas != false ? $datas->sertkNama : '' ?>">
                        </div>
                        <div class="form-group">
                            <label>
                                Draft Sertifikat
                            </label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="permintaanSertifikatDraft" id="custom-file-input">
                                <label class="custom-file-label" for="customFile">
                                    Pilih Berkas
                                </label>
                                <span class="m-form__help">
                                    Filetipe: *.pdf <?= $datas != false ? (!empty($datas->permintaanSertifikatDraft) ? (' | <a href="' . $loadpdf_url . $this->encryptions->encode(json_encode([$datas->permintaanNoOrder, $datas->permintaanSertifikatDraft]), $this->config->item('encryption_key')) . '"> Draft Sertifikat </a>') : '') : '' ?>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->