<?php $this->load->view('layouts/subheader'); ?>
<div id='main_form'>
    <div id="first-form">
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title"> <?= strtoupper($page_judul) ?></h3>
                            </div>
                        </div>
                        <form class="kt-form" action="<?= $tampil ?>" method="post" id="form_show">
                            <div class="kt-portlet__body">
                                <div class="form-group">
                                    <label>Tanggal Permintaan</label>
                                    <!-- <div class="col-lg-12 col-md-9 col-sm-12"> -->
                                    <div class="input-daterange input-group" id="kt_datepicker_5">
                                        <input type="text" class="form-control" name="tanggalawal" autocomplete="off"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                        </div>
                                        <input type="text" class="form-control" name="tanggalakhir" autocomplete="off"/>
                                    </div>
                                    <span class="form-text text-muted">Tanggal Awal dan Tanggal Akhir</span>                                 
                                </div>                        
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <button type="submit" id="btn_form" class="btn btn-primary">Show</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="response" class=""></div>
    </div>    
</div>