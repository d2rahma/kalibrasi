<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">

                <div class="kt-portlet__body">
                    <div class="kt-portlet__head-title">
                        <h5><?= strtoupper($page_judul) ?></h5>
                    </div>
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Nomor Order</th>
                                            <th>Identitas Pelanggan</th>                 
                                            <th>Tanggal Permintaan</th>
                                            <th>Status Sertifikat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            foreach ($datas as $row) {
                                                $key = $this->encryptions->encode($row->permintaanId, $this->config->item('encryption_key'));
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->permintaanNoOrder ?></td>
                                                    <td><?=$row->customerNama.'<br>'.$row->customerNoHp.'<br>'.$row->customerEmail?></td>               
                                                    <td><?= $row->permintaanTgl ?></td>
                                                    <td><?= $row->statusNama ?></td>
                                                    <td>
                                                        <?php
                                                        if ($row->permintaanStatus == 6 or $row->permintaanStatus == 12) {
                                                        ?>
                                                            <a href="<?= $prosesSertifikat_url . $key ?>" title="Proses Sertifikat" id='proses_sert<?= $i; ?>' class="proses_sert btn btn-sm btn-outline-warning btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="far fa-clipboard"></i>
                                                                </span>
                                                            </a>
                                                        <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($row->permintaanStatus == 13) {
                                                        ?>
                                                            <a href="<?= $selesaiSertifikat_url . $key ?>" title="Sertifikat Selesai" id='selesai_sert<?= $i; ?>' class="selesai_sert btn btn-sm btn-outline-success btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fas fa-clipboard-check"></i>
                                                                </span>
                                                            </a>
                                                        <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($row->permintaanStatus == 14 and in_array($susrSgroupNama, ['SEKRETARIAT', 'ADMIN'])) {
                                                        ?>
                                                            <a href="<?= $acceptcust_url . $key ?>" title="Terima Sertifikat" id='accept_cust<?= $i; ?>' class="accept_cust btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fas fa-paper-plane"></i>
                                                                </span>
                                                            </a>
                                                        <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->