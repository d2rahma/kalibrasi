<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">

                <div class="kt-portlet__body">
                    <div class="kt-portlet__head-title">
                        <h5><?= strtoupper($page_judul) ?></h5>
                    </div>
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Nomor Order</th>
                                            <th>Identitas Customer</th>                 
                                            <th>Tanggal Permintaan</th>
                                            <th>Status Sertifikat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            foreach ($datas as $row) {
                                                $key = $this->encryptions->encode($row->permintaanId, $this->config->item('encryption_key'));
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->permintaanNoOrder ?></td>
                                                    <td><?=$row->customerNama.'<br>'.$row->customerNoHp.'<br>'.$row->customerEmail?></td>               
                                                    <td><?= $row->permintaanTgl ?></td>
                                                    <td><?= $row->statusNama ?></td>
                                                    <td>
                                                        <?php
                                                        if ($row->permintaanStatus == 6) {
                                                        ?>
                                                            <a href="<?= $draft_url . $key ?>" title="Upload Draft" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fas fa-arrow-up"></i>
                                                                </span>
                                                            </a>
                                                        <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if (!empty($row->permintaanSertifikatDraft) and empty($row->permintaanSertifikatFinal)) {
                                                            $keyDraft = $this->encryptions->encode(json_encode([$row->permintaanNoOrder, $row->permintaanSertifikatDraft]), $this->config->item('encryption_key'));
                                                        ?>
                                                            <a href="<?= $loadpdf_url . $keyDraft ?>" title="Download Draft" class="btn btn-sm btn-outline-success btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fas fa-arrow-down"></i>
                                                                </span>
                                                            </a>
                                                        <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if (!empty($row->permintaanSertifikatFinal)) {
                                                            $keyFinal = $this->encryptions->encode(json_encode([$row->permintaanNoOrder, $row->permintaanSertifikatFinal]), $this->config->item('encryption_key'));
                                                        ?>
                                                            <a href="<?= $loadpdf_url . $keyFinal ?>" title="Download Final" class="btn btn-sm btn-outline-success btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fas fa-arrow-down"></i>
                                                                </span>
                                                            </a>
                                                        <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($row->permintaanStatus == 7 and in_array($susrSgroupNama, ['PENYELIA', 'ADMIN'])) {
                                                        ?>
                                                            <a href="<?= $verifypy_url . $key ?>" title="Validasi Penyelia Terima" id='verify_py<?= $i; ?>' class="verify_py btn btn-sm btn-outline-warning btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fas fa-check"></i>
                                                                </span>
                                                            </a>
                                                            <a href="<?= $verifypyt_url . $key ?>" title="Validasi Penyelia Tolak" id='verify_pyt<?= $i; ?>' class="verify_pyt btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fas fa-exclamation-triangle"></i>
                                                                </span>
                                                            </a>
                                                        <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($row->permintaanStatus == 8 and in_array($susrSgroupNama, ['KASI', 'ADMIN'])) {
                                                        ?>
                                                            <a href="<?= $verifymt_url . $key ?>" title="Validasi Manajer Teknis" id='verify_mt<?= $i; ?>' class="verify_mt btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fas fa-check"></i>
                                                                </span>
                                                            </a>
                                                        <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($row->permintaanStatus == 9 and in_array($susrSgroupNama, ['KAUPTD', 'ADMIN'])) {
                                                        ?>
                                                            <a href="<?= $verifykp_url . $key ?>" title="Validasi Kepala UPTD" id='verify_kp<?= $i; ?>' class="verify_kp btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fas fa-check-double"></i>
                                                                </span>
                                                            </a>
                                                        <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($row->permintaanStatus == 10 and in_array($susrSgroupNama, ['SEKRETARIAT', 'ADMIN'])) {
                                                        ?>
                                                            <a href="<?= $acceptcust_url . $key ?>" title="Terima Sertifikat" id='accept_cust<?= $i; ?>' class="accept_cust btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fas fa-paper-plane"></i>
                                                                </span>
                                                            </a>
                                                        <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->