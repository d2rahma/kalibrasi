"use strict";
// Class Definition
var FormCustom = function() {
   var handleClickDelete = function() 
   {
        $(".ts_remove_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');

            swal.fire({
                title: "Apakah Anda Yakin Akan Hapus Data?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Hapus!"
            }).then(function(e) {
                e.value && 
                $.ajax(
                {
                    url:$(idLink).attr('href'),
                    success:function(data) 
                    {
                        var res = $.parseJSON(data);
                        $('#response').fadeIn('slow').html(res.response);
                        swal.fire({title: "Deleted!", text: res.message, type: res.status}).then(
                            function(){ 
                                location.reload();
                            }
                            ) ;                   
                    }
                });
            })    
        });
    }
    var handleClickVerify = function() {
    $(".ts_verify_row").click(function(e) {
        e.preventDefault();
        var idLink = '#'+$(this).attr('id');
        swal.fire({
            title: "Apakah Anda Yakin Akan Verifikasi Data Ini?",
            text: "Data Yang Telah Diverifikasi Akan Diproses!!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, Verifikasi!"
        }).then(function(e) {
            e.value && 
            $.ajax(
            {
                url:$(idLink).attr('href'),
                success:function(data) 
                {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({title: "Verifikasi!", text: res.message, type: res.status}).then(
                        function(){ 
                            location.reload();
                        }
                        ) ;                   
                }
            });
        })    
    });}

    var handleClickDisposisi = function() {
    $(".ts_disposisi_row").click(function(e) {
        e.preventDefault();
        var idLink = '#'+$(this).attr('id');

        swal.fire({
            title: "Apakah Anda Yakin Akan Disposisi Data Ini?",
            text: "Data Yang Telah Didisposisi Akan Diproses!!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, Disetujui!"
        }).then(function(e) {
            e.value && 
            $.ajax(
            {
                url:$(idLink).attr('href'),
                success:function(data) 
                {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({title: "Disposisi!", text: res.message, type: res.status}).then(
                        function(){ 
                            location.reload();
                        }
                        ) ;                   
                }
            });
        })    
    });
    }

    var handleClickAlat = function() {
    $(".ts_alat_row").click(function(e) {
        e.preventDefault();
        var idLink = '#'+$(this).attr('id');

        swal.fire({
            title: "Apakah Alat Telah Dikembalikan Kepada Pelanggan?",
            text: "Status Alat : Telah Dikembalikan!!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function(e) {
            e.value && 
            $.ajax(
            {
                url:$(idLink).attr('href'),
                success:function(data) 
                {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({title: "Alat Dikembalikan!", text: res.message, type: res.status}).then(
                        function(){ 
                            location.reload();
                        }
                        ) ;                   
                }
            });
        })    
    });
    }



    var handleSubmit = function(form) {
        $('#response').html('');
        var button = $('#btn_form');
        var button_text = button.text();
        button.prop( "disabled", true );
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function(data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch(err)
                {
                    $('#response').fadeIn('slow').html(data);
                }
                button.prop( "disabled", false );
                button.removeClass('disabled');
                button.text(button_text);       
                FormCustom.init();    
            }
        })
    }

    var handleSubmitFormShow = function() {
        $("#form_show").validate({
            rules: {
                tanggalawal: {
                    required: true
                },
                tanggalakhir: {
                    required: true
                }
            },
            submitHandler: function(e) {
                handleSubmit(e);
                return false
            }
        });

    }

    var handleSubmitForm = function() {
        $("#form_permintaan").validate({
            rules: {
                permintaanCustomerId: {
                    required: true
                }            
            },
            message:{
                    permintaanCustomerId:{ 
                        required : 'Nama Pelanggan/Perusahaan Harus Dipilih'
                    }                
                },
            submitHandler: function(e) {
                handleSubmit(e);
                return false
            }
        });
    }


    var formrepeat = function() {
        $('#kt_repeater_3').repeater({
            initEmpty: false,

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });
    }

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    } 
    var tanggal = function () {

        $('#kt_datepicker_5').datepicker({
            format: "yyyy-mm-dd",
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            autoclose: true,
            orientation: "bottom right",
            templates: arrows
        });  
    }
    return {
            // public functions
            init: function() {
                handleSubmitFormShow();
                handleClickDelete();
                handleClickVerify();
                handleClickDisposisi();
                handleClickAlat();
                handleSubmitForm();
                tanggal(); 
                formrepeat();
            }
        };
}();

jQuery(document).ready(function() {
    FormCustom.init()
});