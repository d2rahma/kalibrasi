"use strict";
// Class Definition
var FormCustom = function() {
   var handleClickDelete = function() {
       $(".ts_remove_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');

            swal.fire({
                title: "Apakah Anda Yakin Akan Hapus Data?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Hapus!"
            }).then(function(e) {
                e.value && 
                $.ajax(
                {
                    url:$(idLink).attr('href'),
                    success:function(data) 
                    {
                        var res = $.parseJSON(data);
                        $('#response').fadeIn('slow').html(res.response);
                        swal.fire({title: "Deleted!", text: res.message, type: res.status}).then(
                            function(){ 
                                location.reload();
                            }
                            ) ;                   
                    }
                });
            })    
        });
    }

    var handleClickStart = function() {
        $(".ts_start_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');

            swal.fire({
                title: "Apakah Anda Yakin Akan Memulai Kalibrasi Ini?",
                text: "Kalibrasi Dimulai!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Mulai Kalibrasi!"
            }).then(function(e) {
                e.value && 
                $.ajax(
                {
                    url:$(idLink).attr('href'),
                    success:function(data) 
                    {
                        var res = $.parseJSON(data);
                        $('#response').fadeIn('slow').html(res.response);
                        swal.fire({title: "Selesai!", text: res.message, type: res.status}).then(
                            function(){ 
                                location.reload();
                            }
                            ) ;                   
                    }
                });
            })    
        });}  

    var handleClickDone = function() {
        $(".ts_done_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');

            swal.fire({
                title: "Apakah Anda Yakin Akan Menyelesaikan Kalibrasi Ini?",
                text: "Kalibrasi Selesai!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Selesai!"
            }).then(function(e) {
                e.value && 
                $.ajax(
                {
                    url:$(idLink).attr('href'),
                    success:function(data) 
                    {
                        var res = $.parseJSON(data);
                        $('#response').fadeIn('slow').html(res.response);
                        swal.fire({title: "Selesai!", text: res.message, type: res.status}).then(
                            function(){ 
                                location.reload();
                            }
                            ) ;                   
                    }
                });
            })    
        });}  




    var handleSubmit = function(form) {
        $('#response').html('');
        var button = $('#btn_form');
        var button_text = button.text();
        button.prop( "disabled", true );
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function(data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch(err)
                {
                    $('#response').fadeIn('slow').html(data);
                }
                button.prop( "disabled", false );
                button.removeClass('disabled');
                button.text(button_text);  
                FormCustom.init();
            }
        })
    }

    var handleSubmitFormShow = function() {
        $("#form_show").validate({
            rules: {
                tanggalawal: {
                    required: true
                },
                tanggalakhir: {
                    required: true
                }
            },
            submitHandler: function(e) {
                handleSubmit(e);
                return false
            }
        });

    }

    var handleSubmitForm = function() {
        $("#form_permintaan").validate({
            rules: {
                permintaanTglAwalPengujian: {
                    required: true
                },
                permintaanTglAkhirPengujian: {
                    required: true,
                },
                petugasPegawaiId: {
                    required: true
                },
                permintaanLokasiPengujian: {
                    required: true
                }
            },
            message:{
                    permintaanTglAwalPengujian:{ 
                        required : 'Tanggal Awal Kalibrasi Harus Diisii'
                    },
                    permintaanTglAkhirPengujian:{
                        required : 'Tanggal Akhir Kalibrasi Harus Diisi',
                    },
                    petugasPegawaiId:{                    
                        required : 'Petugas Kalibrasi Harus Diisi'
                    },
                   
                    permintaanLokasiPengujian:{
                        required : 'Lokasi Kalibrasi Harus Dipilih'
                    }
                },
            submitHandler: function(e) {
                handleSubmit(e);
                return false
            }
        });
    }

    var handleSubmitFormSelesai = function() {
        $("#form_penyelesaian").validate({
            rules: {
                alatJumlahSelesai: {
                    required: true
                }
            },
            message:{
                    alatJumlahSelesai:{ 
                        required : 'Alat Selesai Harus Diisi'
                    }
                },
            submitHandler: function(e) {
                handleSubmit(e);
                return false
            }
        });
    }

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    } 
    var tanggal = function () {

        $('#kt_datepicker_5').datepicker({
            format: "yyyy-mm-dd",
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            autoclose: true,
            orientation: "bottom right",
            templates: arrows
        });  
    }

    var ceknilai = function () {
        
        $(".alatJumlahSelesai").on('keyup', function(){             
            var alatJumlahSelesai = $('.alatJumlahSelesai').map(function(){
              return +this.value;            
            }).get();        

            var alatJumlah = $('.alatJumlah').map(function(){
              return +this.value;       
            }).get();        

            var i=0;
            for(i=0;i<alatJumlahSelesai.length;i++)
            {
                var alatSelesai =alatJumlahSelesai[i];
                var alat = alatJumlah[i]; 

                if(alatSelesai > alat)
                {                    
                    swal.fire('Jumlah Alat Selesai Tidak Boleh Lebih Besar dari Jumlah Alat');                    
                }                
            }
            //if (idalatSelesai.val() > idalat.val())
            //alert("Jumlah Alat Selesai Tidak Boleh Lebih Besar dari Jumlah Alat");

            return false;
        });
    }

    return {
            // public functions
            init: function() {
                handleSubmitFormShow();
                handleClickDelete();        
                handleClickDone();
                handleClickStart();
                handleSubmitForm();
                handleSubmitFormSelesai();
                tanggal(); 
                ceknilai();
            }
        };
    }();

    jQuery(document).ready(function() {
        FormCustom.init()
});