"use strict";
// Class Definition
var FormCustom = function () {
    var handleClickVerifyMt = function () {
        $(".verify_mt").click(function (e) {
            e.preventDefault();
            var idLink = '#' + $(this).attr('id');

            swal.fire({
                title: "Apakah Manajer Teknis Yakin Akan Tanda Tangan Sertifikat Ini?",
                text: "Sertifikat Akan Diproses!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Tanda Tangan!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        url: $(idLink).attr('href'),
                        success: function (data) {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                title: "Disposisi!",
                                text: res.message,
                                type: res.status
                            });
                        }
                    });
            })
        });
    }

    var handleClickVerifyPY = function () {
        $(".verify_py").click(function (e) {
            e.preventDefault();
            var idLink = '#' + $(this).attr('id');

            swal.fire({
                title: "Apakah Penyelia yakin verifikasi Sertifikat Ini?",
                text: "Sertifikat Akan Diproses!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Verifikasi!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        url: $(idLink).attr('href'),
                        success: function (data) {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                title: "Disposisi!",
                                text: res.message,
                                type: res.status
                            });
                        }
                    });
            })
        });
    }

    var handleClickVerifyPYT = function () {
        $(".verify_pyt").click(function (e) {
            e.preventDefault();
            var idLink = '#' + $(this).attr('id');

            swal.fire({
                title: "Apakah Penyelia yakin TOLAK Sertifikat Ini?",
                text: "Sertifikat Akan Dikembalikan ke Draft!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Tolak!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        url: $(idLink).attr('href'),
                        success: function (data) {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                title: "Disposisi!",
                                text: res.message,
                                type: res.status
                            });
                        }
                    });
            })
        });
    }

    var handleClickVerifyKP = function () {
        $(".verify_kp").click(function (e) {
            e.preventDefault();
            var idLink = '#' + $(this).attr('id');

            swal.fire({
                title: "Apakah Kepala UPTD Yakin Akan Tanda Tangan Sertifikat Ini?",
                text: "Sertifikat Akan Diproses!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Tanda Tangan!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        url: $(idLink).attr('href'),
                        success: function (data) {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                title: "Disposisi!",
                                text: res.message,
                                type: res.status
                            });
                        }
                    });
            })
        });
    }

    var handleClickVerifyAccCust = function () {
        $(".accept_cust").click(function (e) {
            e.preventDefault();
            var idLink = '#' + $(this).attr('id');

            swal.fire({
                title: "Apakah Sertifikat Ini Telah Diterima oleh Customer ?",
                text: "Sertifikat Selesai!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Proses!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        url: $(idLink).attr('href'),
                        success: function (data) {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                title: "Sertifikat Selesai!",
                                text: res.message,
                                type: res.status
                            });
                        }
                    });
            })
        });
    }

    var handleSubmit = function (form) {
        $('#response').html('');
        var button = $('#btn_form');
        var button_text = button.text();
        button.prop("disabled", true);
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function (data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch (err) {
                    $('#response').fadeIn('slow').html(data);
                }
                button.prop("disabled", false);
                button.removeClass('disabled');
                button.text(button_text);
                FormCustom.init();
            }
        })
    }

    var handleSubmitFormShow = function () {
        $("#form_show").validate({
            rules: {
                tanggalawal: {
                    required: true
                },
                tanggalakhir: {
                    required: true
                }
            },
            submitHandler: function (e) {
                handleSubmit(e);
                return false
            }
        });

    }

    var handleSubmitForm = function () {
        $("#form_data").validate({
            rules: {

            },
            message: {

            },
            submitHandler: function (form) {


                $('#response').html('');
                var button = $('#btn_save');
                var button_text = button.text();
                button.prop("disabled", true);
                button.addClass('disabled');
                button.text('Sedang Memproses...');

                var forms = $("#form_data")[0];
                var form_data = new FormData(forms);

                $.ajax({
                    type: $("#form_data").attr('method'),
                    url: $("#form_data").attr('action'),
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        try {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                position: "top-right",
                                type: res.status,
                                title: res.message,
                                showConfirmButton: !1,
                                timer: 1500
                            });
                        } catch (err) {
                            $('#response').fadeIn('slow').html(data);
                        }
                        button.prop("disabled", false);
                        button.removeClass('disabled');
                        button.text(button_text);
                        FormCustom.init();
                    }
                });
                return false
            }
        });
    }

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    var tanggal = function () {

        $('#kt_datepicker_5').datepicker({
            format: "yyyy-mm-dd",
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            autoclose: true,
            orientation: "bottom right",
            templates: arrows
        });
    }

    var loadklassert = function () {
        $("#permintaanNoSert").change(function () {
            var id = $("#permintaanNoSert").val();
            $.ajax({
                type: 'post',
                url: '/reqkalsertifikat/loadklassert/' + id,
                data: 'id=' + id,
                success: function (data) {
                    $('#klasSertifikat').val(data);
                }
            });
            return false;
        });
    }

    var handleClickProsesSert = function () {
        $(".proses_sert").click(function (e) {
            e.preventDefault();
            var idLink = '#' + $(this).attr('id');

            swal.fire({
                title: "Apakah Sertifikat Akan Diproses?",
                text: "Proses Sertifikat!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Proses Sertifikat!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        url: $(idLink).attr('href'),
                        success: function (data) {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                title: "Proses Sertifikat!",
                                text: res.message,
                                type: res.status
                            });
                        }
                    });
            })
        });
    }
    var handleClickSelesaiSert = function () {
        $(".selesai_sert").click(function (e) {
            e.preventDefault();
            var idLink = '#' + $(this).attr('id');

            swal.fire({
                title: "Apakah Sertifikat Telah Selesai?",
                text: "Sertifikat Selesai!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Sertifikat Selesai!"
            }).then(function (e) {
                e.value &&
                    $.ajax({
                        url: $(idLink).attr('href'),
                        success: function (data) {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                title: "Sertifikat Selesai!",
                                text: res.message,
                                type: res.status
                            });
                        }
                    });
            })
        });
    }
    return {
        // public functions
        init: function () {
            handleSubmitFormShow();
            handleClickVerifyPY();
            handleClickVerifyPYT();
            handleClickVerifyMt();
            handleClickVerifyKP();
            handleClickVerifyAccCust();
            handleSubmitForm();
            tanggal();
            loadklassert();
            handleClickSelesaiSert();
            handleClickProsesSert();
        }
    };
}();

jQuery(document).ready(function () {
    FormCustom.init()
});