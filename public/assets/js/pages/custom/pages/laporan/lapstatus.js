"use strict";
// Class Definition
var FormCustom = function () {

    var handleSubmit = function (form) {
        $('#response').html('');
        var button = $('#btn_save');
        var button_text = button.text();
        button.prop("disabled", true);
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function (data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch (err) {
                    $('#response').fadeIn('slow').html(data);
                }
                button.prop("disabled", false);
                button.removeClass('disabled');
                button.text(button_text);
                FormCustom.init();
            }
        })
    }

    var handleSubmitFormShow = function () {
        $("#form_show").validate({
            rules: {
                tanggalawal: {
                    required: true
                },
                tanggalakhir: {
                    required: true
                }
            },
            submitHandler: function (e) {
                handleSubmit(e);
                return false
            }
        });

    }


    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    var tanggal = function () {

        $('#kt_datepicker_5').datepicker({
            format: "yyyy-mm-dd",
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            autoclose: true,
            orientation: "bottom right",
            templates: arrows
        });
    }

    return {
        // public functions
        init: function () {
            handleSubmitFormShow();
            tanggal();
        }
    };
}();

jQuery(document).ready(function () {
    FormCustom.init()
});