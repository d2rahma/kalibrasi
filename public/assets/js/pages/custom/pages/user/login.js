"use strict";

// Class Definition
var KTLoginGeneral = function() {

    var login = $('#kt_login');

    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div class="alert alert-' + type + ' alert-dismissible" role="alert">\
			<div class="alert-text">'+msg+'</div>\
			<div class="alert-close">\
                <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>\
            </div>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }

     var displayTrackForm = function () {
        $('#kt-login--track').show();
        $('#kt-login--signin').hide();    
        login.removeClass('kt-login__signin');
        login.addClass('kt-login__track');
        //login.find('.kt-login--forgot').animateClass('flipInX animated');
        KTUtil.animateClass(login.find('.kt-login__track')[0], 'flipInX animated');

    }
    var displaySignInForm = function () {
        $('#kt-login--track').hide();        
        $('#kt-login--signin').show();
        
        login.removeClass('kt-login__track');
        login.addClass('kt-login__signin');
        KTUtil.animateClass(login.find('.kt-login__signin')[0], 'flipInX animated');
        //login.find('.kt-login__signin').animateClass('flipInX animated');
    }

    $('#kt_tracking').click(function (e) {
        e.preventDefault();
        displayTrackForm();
    });

    $('#kt_signin').click(function (e) {
        e.preventDefault();
        displaySignInForm();
    });

    var handleSignInFormSubmit = function() {
        $('#kt_signin_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize(),
                success: function(response, status, xhr, $form) {
                    var eR = $.parseJSON(response);

                	setTimeout(function() {
	                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
	                    showErrorMsg(form, eR.status, eR.message);
                    }, 1e3);

                    if(eR.status=='success')
                    {
                        setTimeout(function(){
                            window.location.href = eR.redirect_url;
                        }, 2e3);
                        
                    }
                }
            });
        });
    }

    var handleTrackFormSubmit = function () {
        $('#kt_track_submit').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $('#kt_track_form');

            form.validate({
                rules: {
                    nomorTiket: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response, status, xhr, $form) {
                    var eR = $.parseJSON(response);

                    setTimeout(function () {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        showErrorMsg(form, eR.status, eR.message);
                    }, 1e3);

                    if (eR.status == 'success') {
                        setTimeout(function () {
                            window.location.href = eR.redirect_url;
                        }, 2e3);

                    }
                }
            });
        });
    }


    // Public Functions
    return {
        // public functions
        init: function() {            
            handleSignInFormSubmit();
            handleTrackFormSubmit();            ;
            displaySignInForm();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTLoginGeneral.init();
});
